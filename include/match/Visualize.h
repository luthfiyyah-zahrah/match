#pragma once

#include <iostream>
#include <thread>
#include <mutex>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/filters/passthrough.h>

#include "match/define_struct.h"

class Visualize{
private:
	std::thread vis;
	cv::Ptr<pcl::visualization::PCLVisualizer> viewer;
	cv::Mat r_vector;
	cv::Mat t_vector;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr mycloud;
	bool end_flag;
	bool update;
	bool path;
	std::mutex visMutex;
	std::vector<std::string> line_name;

	void drawing_path_area();
	void convert(cv::Mat r_vec, cv::Mat t_vec, pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud);
	void visualize_multi();

public:
	Visualize();
	~Visualize();

	void run(cv::Mat r_vec, cv::Mat t_vec, pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud);
	void finish();

	_path_estimate Path_area;
	bool draw_path;

};