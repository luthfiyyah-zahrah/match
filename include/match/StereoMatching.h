
#pragma once
#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/surface/mls.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include "opencv2/opencv.hpp"
#include "match/define_struct.h"


class StereoMatching{
private:
	int ndisp;
	int sad_blocksize;
	double grid_size;//mm
	double map_size_x;//mm
	double map_size_y;
	cv::Mat xyz;
	cv::Mat repro_Q;
	cv::Mat disp;
	cv::Ptr<cv::StereoSGBM> sgbm;
	cv::Mat ground_R, ground_t;
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree;
	pcl::ModelCoefficients::Ptr plane_coeffs;

	struct _estimate_area_info{
		std::vector<double> height;
		std::vector<int> point_num;
	};

	void MLS();
	void downsampling();
	void detect_plane();
	void outlier_removal();
	void create_estimated_path_area(cv::Mat r_vector, cv::Mat t_vector);
	void compute_height();
	double detect_highest_point(_estimate_area_info estimate_area);
	double smirnov_grubbs(std::vector<double> height_candidate);
	void set_params();

public:
	cv::Mat disp_vis;
	double height;
	_path_estimate Path_area;

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud;

	StereoMatching(cv::Mat Q, cv::Size imgsize);
	~StereoMatching();

	void run(std::vector<cv::Mat> img_g, cv::Mat img_o);
	void createPointCloud(cv::Mat img_orig, cv::Mat R, cv::Mat tvec);
};
