#ifndef COMPUTE_POSE_H
#define COMPUTE_POSE_H

#include <math.h>
#include <thread>
// #include "match/PointDetector.h"
#include "match/Data_struct.h"

class compute_pose
{
    public:
        double mFPS;

	sPose left_cam_pose;

	bool DoingStereo;

	
	compute_pose(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length);
        virtual ~compute_pose();
    void run(std::vector<cv::Mat> img);
	// bool operation(const int key);
	// void draw(cv::Mat &img);
    protected:


    private:
    sPose mPose;
	cv::Mat G_img;

	// PointDetector detector;
	std::string mText;

	cv::Mat left_cam;
	cv::Mat right_cam;
	cv::Mat right_cam_g;
	sPose right_Pose;
	Data mdata;
	void init_params(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length);
    void compute();
};

#endif // COMPUTE_POSE_H
