#pragma once
#include <iostream>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <thread>
#include "match/PointDetector.h"
#include "match/Data_struct.h"

#include "cvsba.h"

class online_sfm{
private:
	sPose mPose;
	cv::Mat G_img;

	PointDetector detector;
	std::string mText;

	cv::Mat left_cam;
	cv::Mat right_cam;
	cv::Mat right_cam_g;
	sPose right_Pose;

	enum STATE{
		STOP, INIT, TAM, RELOCAL, CLOSE
	};
	STATE mState;
	Data mdata;

	cvsba::Sba mBA;

	std::thread BA;
	bool mDoingBA;			//!< doing BA or not
	std::mutex mBAMutex;	//!< for BA

	//setting_of_camera
	void init_params(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length);

	//process
	void drawProcess(cv::Mat &img) const;
	void startInit();
	void changeState();
	void reset();

	//tracking
	bool setKeyframe();
	void projectMap();
	bool checkInsideImage(const cv::Point2f &pt) const;
	int trackFrame();
	void drawTrack(cv::Mat &img) const;
	bool matchKeyframe();
	bool computePose();

	//mapping
	void triangulate(const std::vector<cv::Point2f> &vpt1, const std::vector<cv::Point2f> &vpt2, const cv::Mat &A, const sPose &pose1, const sPose &pose2, std::vector<cv::Point3f> &vpt3d) const;
	void drawMap(cv::Mat &img) const;
	bool mappingCriteria() const;
	void mapping();
	void trackAndMap();
	bool initialBA(std::vector<cv::Point3f> &vpt3d, const std::vector<cv::Point2f> &vundist1, const std::vector<cv::Point2f> &vundist2, const cv::Mat &A, sPose &pose1, sPose &pose2);
	bool makeMap();
	bool makeMap_init();
	void initialize();
	double sad(cv::Mat left, cv::Mat right);

	//relocalization
	void changeRelocalImage();
	void relocalize();
	void drawView(cv::Mat &img);

	//local Bundle Adjustment
	void localBA();

public:
	double mFPS;

	sPose left_cam_pose;

	bool DoingStereo;

	online_sfm(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length);
	~online_sfm();
	void run(std::vector<cv::Mat> img);
	bool operation(const int key);
	void draw(cv::Mat &img);
	void finish();
};
