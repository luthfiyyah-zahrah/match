#pragma once

#include <string>
#include <vector>

#include <chrono>

class Timer
{
public:
	int Pop(const bool flag = false);
	void Push(const std::string &name);

private:

	struct sTask{
		std::string name;
		std::chrono::system_clock::time_point stime;
	};

	std::vector<sTask> mTasks;
};
