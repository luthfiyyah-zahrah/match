#pragma once
#include <opencv2/opencv.hpp>

#include <vector>
#include <list>
#include <map>
#include <mutex>

#ifdef _DEBUG
#define LOGOUT(fmt, ...) printf(fmt, ##__VA_ARGS__) 
#else
#define LOGOUT(fmt, ...)
#endif

struct sPose
{
	sPose();
	sPose(const sPose& r);

	sPose& operator=(const sPose& r);

	void print() const;

	cv::Mat rvec;		//!< 3x1 rotation vector
	cv::Mat tvec;		//!< 3x1 translation vector
	cv::Mat R;
};


struct sTrack
{
	sTrack();

	std::vector<cv::Point2f> vpt;	//!< list of points
	cv::KeyPoint kpt;				//!< keypoint at first frame
	int ptID;						//!< point ID
};

struct sKeyframe
{
	void clear();

	cv::Mat img;	//!< image
	sPose pose;		//!< pose

	// for relocalization
	std::vector<int> vkptID;			//!< point ID 
	std::vector<cv::KeyPoint> vkpt;		//!< keypoints
	cv::Mat vdesc;						//!< descriptors

	// for ba
	std::vector<int> vptID;			//!< point ID
	std::vector<cv::Point2f> vpt;	//!< points

	int ID;		//!< keyframe ID
};

struct sBAData
{
	void clear();

	std::vector<cv::Point3f> vpt3d;		//!< mapped points
	std::vector<int> vvisibleID;		//!< IDs of visible mapped points
	std::vector<sKeyframe> vkeyframe;	//!< keyframes
	std::vector<int> vkeyframeID;		//!< IDs of keyframes
};

class MapData
{
public:
	MapData();

	void Clear();
	bool CopytoBA(sBAData &data);
	void CopyfromBA(const sBAData &data);
	void AddKeyframe(const sKeyframe &kf, const cv::Mat &vdesc);
	sKeyframe& GetLastKeyframe();
	void UpdateLastKeyframe(const std::vector<cv::Point3f> &vpt3d, const std::vector<cv::KeyPoint> &vkpt, const cv::Mat &vdesc, std::vector<int> &vid);
	void UpdateLastKeyframe(const std::vector<cv::Point3f> &vpt3d, const std::vector<cv::KeyPoint> &vkpt, const cv::Mat &vdesc);
	const cv::Point3f& GetPoint(const int id) const;
	const sKeyframe& GetNearestKeyframe(const sPose &pose) const;

	void GetPoseforRelocalization(sPose &pose) const;
	void GetGoodPoseforRelocalization(sPose &pose) const;
	int GetSize() const;

private:
	std::vector<cv::Point3f> mvPt;	//!< 3D points in map
	std::vector<sKeyframe> mvKf;	//!< keyframes
	std::mutex mMapMutex;				//!< mutex
	bool mAdded;					//!< keyframe added and not used in BA
};

struct Params
{
	Params();

	int MAXPTS;					// maximum number of points
	int LEVEL;					// pyramid level for detector
	float DESCDIST;				// max distance for correct matches

	double BASEANGLE;			//!< angle (degree) for adding new keyframe
	double BASETAN;				//!< tangent of BASEANGLE
	int BAKEYFRAMES;			// number of keyframes for local bundle adjustment

	float PROJERR;				// tolerance for reprojection error
	int MINPTS;					// minimum number of points for tracking and mapping
	int PATCHSIZE;				// patch size for KLT	
	float MATCHKEYFRAME;		// inlier ratio for matching keyframe

	float GOODINIT;				// inlier ration for initialization
	int RELOCALHIST;			// check last n frames for relocalization
};

struct Data
{
	Data(void);

	void clear();
	void clearTrack();
	void clearTrack(const int ID);
	void addTrack(const sTrack &in);

	cv::Mat previmg;						//!< previous image
	std::list<sTrack> vtrack;				//!< all tracks	
	std::vector<cv::Point2f> vprevpt;		//!< tracked points in previous image
	double quality;							//!< tracking quality

	MapData map;							//!< data for mapping
	sBAData baData;							//!< data for BA

	cv::Mat A, D;							//!< camera parameters
	double focal;							//!< focal length

	double scale;							//!< scale (world/local)
	bool havescale;							//!< scale is computed or not

	std::vector<cv::KeyPoint> vKpt;			//!< keypoints in current image
};
const int NOID = -1;
const int DISCARD = -2;