#pragma once

#include <opencv2/features2d.hpp>

class PointDetector{
public:
	PointDetector();
	~PointDetector();

	void Init(const int numpts, const int numlevel);
	void Match(const cv::Mat &query, const cv::Mat &train, std::vector<cv::DMatch> &vmatch) const;
	void Describe(const cv::Mat &img, std::vector<cv::KeyPoint> &vkpt, cv::Mat &vdesc) const;
	void Detect(const cv::Mat &img, std::vector<cv::KeyPoint> &vkpt) const;
	void Detect(const cv::Mat &img, std::vector<cv::KeyPoint> &vkpt, const cv::Mat &mask) const;

private:
	cv::Ptr<cv::ORB> mDetector;
	cv::Ptr<cv::DescriptorMatcher> mMatcher;	
};

