#include "match/online_sfm.h"

extern Params params;

online_sfm::online_sfm(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length)
{
	// mDoingBA = false;
	DoingStereo = false;
	reset();
	init_params(A, D, img_size, baseline_length);
	// BA = std::thread(&online_sfm::localBA, this);
}

online_sfm::~online_sfm()
{
}

void online_sfm::init_params(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length)
{
	// set camera parameters to ATAM
	A.copyTo(mdata.A);					// camera paramters	
	D.copyTo(mdata.D);					// distortion parameters
	mdata.focal = mdata.A.at<double>(0,0);	// focal length

	//using_only_initialize
	right_Pose.rvec.setTo(0);
	right_Pose.tvec.setTo(0);
	right_Pose.tvec.at<double>(0, 0) = baseline_length;

	//set initial pose
	mPose.rvec.setTo(0);
	mPose.tvec.setTo(0);
	left_cam_pose.rvec.setTo(0);
	left_cam_pose.tvec.setTo(0);

	// image
	G_img = cv::Mat(img_size, CV_8UC1);
	right_cam_g = cv::Mat(img_size, CV_8UC1);

	// initialize keypoint detector
	detector.Init(params.MAXPTS, params.LEVEL);
}

void online_sfm::drawProcess(cv::Mat &img) const
{
	// draw process at the bottom
	cv::Scalar textcol(0, 255, 0);
	cv::putText(img, mText, cv::Point(0, img.rows - 5), cv::FONT_HERSHEY_SIMPLEX, 0.8, textcol, 2);

	// draw fps	at the top
	cv::Scalar fpscol(0, 255, 0);
	cv::putText(img, std::to_string(int(mFPS)) + " FPS", cv::Point(0, 15), cv::FONT_HERSHEY_SIMPLEX, 0.5, fpscol, 1);
}

void online_sfm::startInit()
{
	// set initial pose
	mPose.rvec.setTo(0);
	mPose.tvec.setTo(0);

	// start keyframe
	setKeyframe();
}

void online_sfm::changeState()
{
	if (mState == STATE::STOP){
		mState = STATE::INIT;
	}
	else if (mState == STATE::TAM){
		DoingStereo = true;
		mText = "Created Dense Map";
	}
}

void online_sfm::reset()
{
	mState = STATE::STOP;
	mFPS = 0.0;

	/*bool tmp = true;

	while (tmp){
		mBAMutex.lock();
		tmp = mDoingBA;
		mBAMutex.unlock();
	}*/

	mdata.clear();

	// change state
	mText = "Press space to start";
	LOGOUT("-------------RESET-------------\n");
}

bool online_sfm::setKeyframe()
{
	mdata.clearTrack(NOID);

	sKeyframe tmpKf;
	tmpKf.pose = mPose;		// keyframe pose

	// set mapped points for BA
	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end(); it != itend; ++it){
		tmpKf.vpt.push_back(it->vpt.back());
		tmpKf.vptID.push_back(it->ptID);
	}

	// select keypoints as new tracks
	std::vector<int> vnewptID;
	std::vector<cv::KeyPoint> &vKpt = mdata.vKpt;

	for (int i = 0, iend = int(vKpt.size()); i < iend; ++i){	// for keypoints

		// check already in track
		bool foundsame = false;
		double mindist = double(params.PROJERR* 2.0f);
		int ID = NOID;

		for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end(); it != itend; ++it){

			double dist = cv::norm(vKpt[i].pt - it->vpt.back());

			if (it->ptID != NOID && dist < mindist){	// if mapped track exists
				ID = it->ptID;
				mindist = dist;
				foundsame = true;
			}
		}

		if (!foundsame){		// if keypoints not in mapped tracks
			vnewptID.push_back(i);
		}
		else{					// if keypoints in mapped tracks
			tmpKf.vkpt.push_back(vKpt[i]);
			tmpKf.vkptID.push_back(ID);
		}
	}

	if (int(vnewptID.size()) < params.MINPTS){	// not suitable if new keypoints are less
		return false;
	}

	// add new points in new tracks
	for (int i = 0, iend = int(vnewptID.size()); i < iend; ++i){
		sTrack tmpTrack;
		tmpTrack.kpt = vKpt[vnewptID[i]];
		tmpTrack.ptID = NOID;
		mdata.addTrack(tmpTrack);
	}

	// compute descriptor
	G_img.copyTo(tmpKf.img);
	cv::Mat vdesc;
	if (tmpKf.vkpt.size() != 0){
		detector.Describe(tmpKf.img, tmpKf.vkpt, vdesc);
	}

	mdata.map.AddKeyframe(tmpKf, vdesc);
	return true;
}

void online_sfm::projectMap()
{
	// select mapped points that are not in tracks
	std::vector<int> vmapID(mdata.map.GetSize());
	for (int i = 0, iend = int(mdata.map.GetSize()); i < iend; ++i){
		vmapID[i] = i;
	}

	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(),	itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID != NOID){
			vmapID[it->ptID] = NOID;
		}
	}

	std::vector<int> vaddID;
	std::vector<cv::Point3f> vaddpt3d;

	for (int i = 0, iend = int(mdata.map.GetSize()); i < iend; ++i){
		if (vmapID[i] != NOID){
			vaddID.push_back(i);
			vaddpt3d.push_back(mdata.map.GetPoint(i));
		}
	}

	if (vaddpt3d.size() > 0){

		// project points to image
		std::vector<cv::Point2f> vaddpt2d;
		cv::projectPoints(vaddpt3d, mPose.rvec, mPose.tvec, mdata.A, mdata.D, vaddpt2d);

		std::vector<cv::KeyPoint> &vKpt = mdata.vKpt;

		int numrecovered = 0;
		for (int i = 0, iend = int(vaddpt2d.size()); i < iend; ++i){

			if (checkInsideImage(vaddpt2d[i])){		// if inside image

				sTrack track;
				bool added = false;
				double mindist = double(params.PROJERR);

				// check projected point is close to a keypoint
				for (int j = 0, jend = int(vKpt.size()); j < jend; ++j){
					double dist = cv::norm(vKpt[j].pt - vaddpt2d[i]);
					if (dist < mindist){
						track.kpt = vKpt[j];
						track.ptID = vaddID[i];
						added = true;
						mindist = dist;
					}
				}

				if (added){

					mdata.addTrack(track);
					++numrecovered;

					// remove new tracks close to the point
					for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {

						if (it->ptID == NOID && cv::norm(track.kpt.pt - it->vpt.back()) < params.PROJERR*2.0f){
							it->ptID = DISCARD;
						}
					}
				}
			}
		}

		mdata.clearTrack(DISCARD);

		if (numrecovered){
			LOGOUT("Recovered %d points at %s\n", numrecovered, __FUNCTION__);
		}
	}
}

bool online_sfm::checkInsideImage(const cv::Point2f &pt) const
{
	if (pt.x < params.PATCHSIZE * 2
		|| G_img.cols - params.PATCHSIZE * 2 < pt.x
		|| pt.y < params.PATCHSIZE * 2
		|| G_img.rows - params.PATCHSIZE * 2 < pt.y){
		return false;
	}
	else{
		return true;
	}
}

int online_sfm::trackFrame()
{
	// LKtracker
	std::vector<cv::Point2f> vtracked;
	std::vector<unsigned char> vstatus;
	std::vector<float> verror;
	const cv::Size patch(params.PATCHSIZE, params.PATCHSIZE);
	const int level = 1;

	cv::TermCriteria criteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 10, 0.1);
	cv::calcOpticalFlowPyrLK(mdata.previmg, G_img, mdata.vprevpt, vtracked, vstatus, verror);

	int count = 0;
	mdata.vprevpt.clear();
	std::list<sTrack>::iterator it = mdata.vtrack.begin();
	for (size_t i = 0; i < vstatus.size(); ++i){
		if (!vstatus[i] || !checkInsideImage(vtracked[i])){	// remove if point not tracked
			it = mdata.vtrack.erase(it);
		}
		else{
			mdata.vprevpt.push_back(vtracked[i]);
			it->vpt.push_back(vtracked[i]);

			if (it->ptID != NOID){
				++count;
			}
			++it;
		}
	}

	return count;
}

void online_sfm::drawTrack(cv::Mat &img) const
{
	const int pointsize = 5;

	const std::list<sTrack> &vtrack = mdata.vtrack;

	cv::Scalar border(0, 0, 0);			// circle border
	cv::Scalar mapped(255, 255, 255);	// mapped
	cv::Scalar newpt(255, 0, 0);		// not mapped

	for (std::list<sTrack>::const_iterator it = vtrack.begin(), itend = vtrack.end();
		it != itend; ++it){
		if (it->ptID != NOID){
			cv::circle(img, it->vpt.back(), pointsize, border, -1);
			cv::circle(img, it->vpt.back(), pointsize - 1, mapped, -1);
			// cv::circle(img, cv::Point(it->vpt.back()), pointsize, border, -1);
			// cv::circle(img, cv::Point(it->vpt.back()), pointsize - 1, mapped, -1);
		}
		else{
			cv::circle(img, it->vpt.back(), pointsize, border, -1);
			cv::circle(img, it->vpt.back(), pointsize - 1, newpt, -1);
		}
	}
}
bool online_sfm::matchKeyframe()
{
	// compute descriptors of current frame
	std::vector<cv::KeyPoint> &vKpt = mdata.vKpt;
	if (vKpt.size() < params.MINPTS){
		return false;
	}
	cv::Mat vdesc;
	detector.Describe(G_img, vKpt, vdesc);

	// get nearest keyframe
	const sKeyframe& kf = mdata.map.GetNearestKeyframe(mPose);
	if (kf.vkpt.size() < params.MINPTS){		// not enough keypoint in keyframe
		return false;
	}

	// matching
	std::vector<cv::DMatch> vmatch;
	detector.Match(vdesc, kf.vdesc, vmatch);

	std::vector<cv::Point3f> vpt3d;
	std::vector<cv::Point2f> vpt2d;
	std::vector<int> vID;

	// select good matches
	for (int i = 0, iend = int(vmatch.size()); i < iend; ++i){
		if (vmatch[i].distance < params.DESCDIST){
			vpt2d.push_back(vKpt[vmatch[i].queryIdx].pt);

			int ID = vmatch[i].trainIdx;
			vpt3d.push_back(mdata.map.GetPoint(kf.vkptID[ID]));
			vID.push_back(kf.vkptID[ID]);
		}
	}

	if (vpt2d.size() > params.MINPTS){	// if enough good correspondences
		// compute camera pose
		sPose tmpPose = mPose;
		const int iteration = 100;
		const double confidence = 0.98;
		std::vector<int> vinliers;

		cv::solvePnPRansac(vpt3d, vpt2d, mdata.A, mdata.D, tmpPose.rvec, tmpPose.tvec, true, iteration, params.PROJERR, confidence, vinliers);
		// check number of inliers and inlier ratio (inlier / all)
		if (int(vinliers.size()) > params.MINPTS && float(vinliers.size()) / float(vpt2d.size()) > params.MATCHKEYFRAME){

			// add as a mapped track
			int numrecovered = 0;
			for (int i = 0, iend = int(vinliers.size()); i < iend; ++i){

				// check already in mapped tracks
				bool found = false;
				int pos = vinliers[i];
				for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {

					if (it->ptID == vID[pos]){
						found = true;
						break;
					}
				}

				if (!found){
					sTrack track;
					track.kpt.pt = vpt2d[pos];
					track.ptID = vID[pos];
					mdata.addTrack(track);
					++numrecovered;
				}
			}

			if (mState == STATE::RELOCAL){
				mPose = tmpPose;
			}

			LOGOUT("Recovered %d points with keyframe %d at %s\n", numrecovered, kf.ID, __FUNCTION__);
			return true;
		}
	}

	return false;
}

bool online_sfm::computePose()
{
	// pose estimation
	std::vector<cv::Point2f> vpt2d;
	std::vector<cv::Point3f> vpt3d;

	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID != NOID){
			vpt2d.push_back(it->vpt.back());
			vpt3d.push_back(mdata.map.GetPoint(it->ptID));
		}
	}

	//三次元点はカメラから計測したものであるため，三次元点の座標系（オブジェクト座標系）とカメラ座標系は一致している．
	//そのため今回の場合，solvePnPの結果はそのままカメラ座標系のものを表している（本当はオブジェクト座標系の物を表す．OpenCVdocumentを読むとわかる）
	cv::solvePnP(vpt3d, vpt2d, mdata.A, mdata.D, mPose.rvec, mPose.tvec, true);

	// check reprojection error and discard points if error is large
	std::vector< cv::Point2f > vrepropt;
	cv::projectPoints(vpt3d, mPose.rvec, mPose.tvec, mdata.A, mdata.D, vrepropt);

	int numall = 0;
	int numdiscard = 0;

	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID != NOID){

			double dist = cv::norm(vrepropt[numall] - vpt2d[numall]);

			if (dist > params.PROJERR){
				it->ptID = DISCARD;
				++numdiscard;
			}
			++numall;
		}
	}

	mdata.clearTrack(DISCARD);
	LOGOUT("Discarded:%d used:%d at %s\n", numdiscard, numall - numdiscard, __FUNCTION__);

	mdata.quality = double(numall - numdiscard) / double(numall);		// inlier ratio

	if (numall - numdiscard > params.MINPTS){
		return true;
	}
	else{
		return false;
	}
}

void online_sfm::triangulate(
	const std::vector<cv::Point2f> &vpt1,
	const std::vector<cv::Point2f> &vpt2,
	const cv::Mat &A,
	const sPose &pose1,
	const sPose &pose2,
	std::vector<cv::Point3f> &vpt3d
	) const
{
	cv::Mat R1, R2;

	cv::Rodrigues(pose1.rvec, R1);
	cv::Rodrigues(pose2.rvec, R2);

	cv::Mat P1(3, 4, R1.type()), P2(3, 4, R2.type());
	R1.copyTo(P1(cv::Rect(0, 0, 3, 3)));
	R2.copyTo(P2(cv::Rect(0, 0, 3, 3)));

	pose1.tvec.copyTo(P1(cv::Rect(3, 0, 1, 3)));
	pose2.tvec.copyTo(P2(cv::Rect(3, 0, 1, 3)));

	P1 = A * P1;
	P2 = A * P2;

	cv::Mat triangulated;

	cv::triangulatePoints(P1, P2, vpt1, vpt2, triangulated);

	vpt3d.resize(vpt1.size());

	for (int i = 0, iend = int(vpt1.size()); i < iend; ++i){

		float x = triangulated.at < float >(0, i);
		float y = triangulated.at < float >(1, i);
		float z = triangulated.at < float >(2, i);
		float w = triangulated.at < float >(3, i);

		vpt3d[i].x = x / w;
		vpt3d[i].y = y / w;
		vpt3d[i].z = z / w;
	}
}

void online_sfm::drawMap(cv::Mat &img) const
{
	const std::list<sTrack> &vtrack = mdata.vtrack;

	std::vector<cv::Point3f> vpt3d;

	// select mapped tracks
	for (std::list<sTrack>::const_iterator it = vtrack.begin(), itend = vtrack.end(); it != itend; ++it){
		if (it->ptID != NOID){
			vpt3d.push_back(mdata.map.GetPoint(it->ptID));
		}
	}

	// project mapped tracks
	if (vpt3d.size() != 0){
		std::vector<cv::Point2f> vpt2d;
		cv::projectPoints(vpt3d, mPose.rvec, mPose.tvec, mdata.A, mdata.D, vpt2d);

		cv::Scalar col(0, 0, 0);		// map color
		const int size = 1;
		for (int i = 0, iend = int(vpt2d.size()); i < iend; ++i){
			// cv::circle(img, cv::Point(vpt2d[i]), size, col, -1);
			cv::circle(img,vpt2d[i], size, col, -1);
		}
	}
}

bool online_sfm::mappingCriteria() const
{
	// middle point between current frame and nearest keyframe
	const sKeyframe &nkf = mdata.map.GetNearestKeyframe(mPose);

	// compute camera location in world coordinate system
	cv::Mat R, nkfR;
	cv::Rodrigues(mPose.rvec, R);
	cv::Rodrigues(nkf.pose.rvec, nkfR);

	cv::Mat pos, mkfPos;
	pos = -R.inv() * mPose.tvec;
	mkfPos = -nkfR.inv() * nkf.pose.tvec;

	double distkeyframe = cv::norm(pos - mkfPos);

	cv::Point3f middle = (cv::Point3f(pos) + cv::Point3f(mkfPos));// / 2.0f;//harusnya 2.0f

	// select median of mapped points
	struct dist3D{
		double dist;
		int ID;
		bool operator< (const dist3D &r) const { return dist < r.dist; }
	};

	std::vector<dist3D> vdist3D;
	for (std::list<sTrack>::const_iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();
		it != itend; ++it){
		if (it->ptID != NOID){
			dist3D tmp;
			tmp.ID = it->ptID;
			tmp.dist = cv::norm(mdata.map.GetPoint(it->ptID) - middle);
			vdist3D.push_back(tmp);
		}
	}

	std::sort(vdist3D.begin(), vdist3D.end());
	const cv::Point3f &median = mdata.map.GetPoint(vdist3D[int(vdist3D.size()) / 2].ID);

	double distpoints = cv::norm(median - middle);

	// mapping criteria
	if (params.BASETAN < distkeyframe / distpoints){
		return true;
	}

	return false;
}

void online_sfm::mapping()
{
	makeMap();

	setKeyframe();
}

void online_sfm::trackAndMap()
{
	bool relocal = false;

	if (mdata.vprevpt.size() < params.MINPTS){		// not enough tracked points
		relocal = true;
	}
	else{

		int mappedpts = trackFrame();		// track points

		if (mappedpts < params.MINPTS){		// not enough mapped points
			relocal = true;					// start relocalization
		}
		else{
			if (!computePose()){
				relocal = true;
			}
			else{
				projectMap();
				matchKeyframe();
				if (mappingCriteria()){	// check mapping criteria
					mapping();			// mapping
				}
			}
		}
	}

	if (relocal){
		mdata.clearTrack();
		mdata.map.GetGoodPoseforRelocalization(mPose);
		LOGOUT("Lost\n");
		mText = "Go back to this view";
		mState = STATE::RELOCAL;
	}
}

/*bool online_sfm::initialBA(
	std::vector<cv::Point3f> &vpt3d,
	const std::vector<cv::Point2f> &vundist1,
	const std::vector<cv::Point2f> &vundist2,
	const cv::Mat &A,
	sPose &pose1,
	sPose &pose2
	)
{
	std::vector< std::vector<cv::Point2f> > imagePoints(2);
	imagePoints[0] = vundist1;
	imagePoints[1] = vundist2;

	std::vector< std::vector<int> > visibility(2);
	std::vector<int> vvis(vundist1.size(), 1);
	visibility[0] = vvis;
	visibility[1] = vvis;

	std::vector<cv::Mat> cameraMatrix(2);
	cameraMatrix[0] = A;
	cameraMatrix[1] = A;

	std::vector<cv::Mat> R(2);
	pose1.rvec.copyTo(R[0]);
	pose2.rvec.copyTo(R[1]);

	std::vector<cv::Mat> T(2);
	pose1.tvec.copyTo(T[0]);
	pose2.tvec.copyTo(T[1]);

	std::vector<cv::Mat> distCoeffs(2);
	distCoeffs[0] = cv::Mat::zeros(cv::Size(1, 5), CV_64F);
	distCoeffs[1] = cv::Mat::zeros(cv::Size(1, 5), CV_64F);

	LOGOUT("initial BA with %d points\n", vpt3d.size());
	double val = mBA.run(vpt3d, imagePoints, visibility, cameraMatrix, R, T, distCoeffs);
	LOGOUT("Initial error = \t %f\n", mBA.getInitialReprjError());
	LOGOUT("Final error = \t %f\n", mBA.getFinalReprjError());

	if (val < 0.0){
		return false;
	}

	R[0].copyTo(pose1.rvec);
	R[1].copyTo(pose2.rvec);

	T[0].copyTo(pose1.tvec);
	T[1].copyTo(pose2.tvec);

	return true;
}*/

double online_sfm::sad(cv::Mat left, cv::Mat right)
{
	int sad = 0;
	for (int j = 0; j < left.rows; j++){
		for (int i = 0; i < left.cols; i++){
			sad += abs((int)right.at<uchar>(j, i) - (int)left.at<uchar>(j, i));
		}
	}
	return sad;
}

bool online_sfm::makeMap(){
	// select start and end points from new tracks
	std::vector<cv::Point2f> vstart;
	std::vector<cv::Point2f> vend;

	std::vector<sTrack*> vnewtrack;
	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID == NOID){
			vstart.push_back(it->vpt[0]);
			vend.push_back(it->vpt.back());
			vnewtrack.push_back(&(*it));
		}
	}

	// if not enough points
	if (vstart.size() < params.MINPTS){
		return false;
	}

	sKeyframe &lkf = mdata.map.GetLastKeyframe();

	// triangulation
	std::vector<cv::Point3f> vpt3d;
	triangulate(vstart, vend, mdata.A, lkf.pose, mPose, vpt3d);

	// check triangulation with reprojection error
	std::vector<cv::Point2f> vppt1, vppt2;
	cv::projectPoints(vpt3d, lkf.pose.rvec, lkf.pose.tvec, mdata.A, cv::Mat(), vppt1);
	cv::projectPoints(vpt3d, mPose.rvec, mPose.tvec, mdata.A, cv::Mat(), vppt2);

	int numinliers = 0;
	std::vector<bool> vinlier(vpt3d.size(), false);
	std::vector<sTrack*>::iterator it = vnewtrack.begin();

	std::vector<cv::Point3f> vinpt3d;
	std::vector<cv::KeyPoint> vinkpt;

	for (int i = 0, iend = int(vpt3d.size()); i < iend; ++i, ++it){

		double dist = cv::norm(vstart[i] - vppt1[i]) + cv::norm(vend[i] - vppt2[i]);

		if (dist < params.PROJERR){
			vinlier[i] = true;
			vinpt3d.push_back(vpt3d[i]);		// 3d cooridnate
			vinkpt.push_back((*it)->kpt);		// keypoint
			++numinliers;
		}
	}

	// add points to keyframe and map
	cv::Mat vindesc;
	if (vinkpt.size() != 0){
		detector.Describe(lkf.img, vinkpt, vindesc);
	}

	std::vector<int> vid;		// ID of points
	mdata.map.UpdateLastKeyframe(vinpt3d, vinkpt, vindesc, vid);

	// set point ID to new tracks
	it = vnewtrack.begin();
	int counter = 0;
	for (int i = 0, iend = int(vinlier.size()); i < iend; ++i, ++it){
		if (vinlier[i]){
			(*it)->ptID = vid[counter];
			++counter;
		}
	}

	return true;
}

bool online_sfm::makeMap_init(){

	std::vector<sTrack*> vnewtrack;
	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		vnewtrack.push_back(&(*it));
	}

	std::vector<cv::KeyPoint> right_cam_kpt;
	detector.Detect(right_cam_g, right_cam_kpt);

	cv::Mat left_desc, right_desc;
	detector.Describe(G_img, mdata.vKpt, left_desc);
	detector.Describe(right_cam_g, right_cam_kpt, right_desc);


	std::vector<cv::DMatch> dmatch;
	detector.Match(left_desc, right_desc, dmatch);

	std::vector<int> inlier_vnewtrack_number;
	std::vector<cv::Point2f> vleft, vright;

	//std::vector<cv::KeyPoint> tmpleft, tmpright;

	for (std::vector<cv::DMatch>::iterator it = dmatch.begin(), itend = dmatch.end(); it != itend; ++it){
		if (checkInsideImage(mdata.vKpt[it->queryIdx].pt) && checkInsideImage(right_cam_kpt[it->trainIdx].pt) && it->distance < params.DESCDIST){
			if (abs(mdata.vKpt[it->queryIdx].pt.y - right_cam_kpt[it->trainIdx].pt.y) < 1){
				vleft.push_back(mdata.vKpt[it->queryIdx].pt);
				vright.push_back(right_cam_kpt[it->trainIdx].pt);
				inlier_vnewtrack_number.push_back(it->queryIdx);



				//tmpleft.push_back(mdata.vKpt[it->queryIdx]);
				//tmpright.push_back(right_cam_kpt[it->trainIdx]);
			}
		}
	}



	//cv::Mat combined_img(cv::Size(G_img.cols, 2 * G_img.rows), CV_8U);
	//cv::Mat dis_img;
	//cv::Rect roi_rect;
	//roi_rect.width = G_img.cols;
	//roi_rect.height = G_img.rows;
	//cv::Mat roi(combined_img, roi_rect);
	//G_img.copyTo(roi);
	//roi_rect.y += G_img.rows;
	//roi = cv::Mat(combined_img, roi_rect);
	//right_cam_g.copyTo(roi);
	//cv::cvtColor(combined_img, combined_img, CV_GRAY2BGR);

	//for (int i = 0; i < tmpleft.size(); ++i){
	//	cv::circle(combined_img, tmpleft[i].pt, 5, cv::Scalar(0, 0, 255), 2);
	//	cv::circle(combined_img, cv::Point(tmpright[i].pt.x, tmpright[i].pt.y + G_img.rows), 5, cv::Scalar(0, 0, 255), 2);
	//	cv::line(combined_img, tmpleft[i].pt, cv::Point(tmpright[i].pt.x, tmpright[i].pt.y + G_img.rows), cv::Scalar(255, 0, 0), 2);
	//}
	//cv::imwrite("keypoints_matching.jpg", combined_img);

	//cv::Mat leftkpvis;
	//cv::drawKeypoints(G_img, mdata.vKpt, leftkpvis, cv::Scalar::all(-1), 4);
	//cv::Mat rightkpvis;
	//cv::drawKeypoints(right_cam_g, right_cam_kpt, rightkpvis, cv::Scalar::all(-1), 4);
	//cv::imwrite("left_keypoints.jpg", leftkpvis);
	//cv::imwrite("right_keypoints.jpg", rightkpvis);

	// if not enough points
	if (vleft.size() < params.MINPTS){
 		return false;
	}

	sKeyframe &lkf = mdata.map.GetLastKeyframe();

	// triangulation
	cv::Mat tmp_R;
	cv::Rodrigues(lkf.pose.rvec, tmp_R);
	sPose mPose_right = lkf.pose;
	cv::Mat tmp_tvec;
	tmp_tvec = (cv::Mat_<double>(3, 1) << tmp_R.at<double>(0, 0) * right_Pose.tvec.at<double>(0, 0) + lkf.pose.tvec.at<double>(0, 0),
		tmp_R.at<double>(1, 0) * right_Pose.tvec.at<double>(0, 0) + lkf.pose.tvec.at<double>(1, 0),
		tmp_R.at<double>(2, 0) * right_Pose.tvec.at<double>(0, 0) + lkf.pose.tvec.at<double>(2, 0));

	mPose_right.tvec = tmp_tvec;
	std::vector<cv::Point3f> vpt3d;
	triangulate(vleft, vright, mdata.A, lkf.pose, mPose_right, vpt3d);

	sPose tmpPose = lkf.pose;
	/*if (!initialBA(vpt3d, vleft, vright, mdata.A, tmpPose, mPose_right)){
		return false;
	}
	else{
		lkf.pose = tmpPose;
	}
*/	//saya ubah
	lkf.pose = tmpPose;

	// check triangulation with reprojection error
	std::vector<cv::Point2f> vppt1, vppt2;
	cv::projectPoints(vpt3d, lkf.pose.rvec, lkf.pose.tvec, mdata.A, cv::Mat(), vppt1);
	cv::projectPoints(vpt3d, mPose_right.rvec, mPose_right.tvec, mdata.A, cv::Mat(), vppt2);

	int numinliers = 0;
	std::vector<bool> vinlier(vpt3d.size(), false);

	std::vector<cv::Point3f> vinpt3d;
	std::vector<cv::KeyPoint> vinkpt;

	for (int i = 0, iend = int(vpt3d.size()); i < iend; ++i){
		double dist = cv::norm(vleft[i] - vppt1[i]) + cv::norm(vright[i] - vppt2[i]);

		if (dist < params.PROJERR && vpt3d[i].z > 0){
			vinlier[i] = true;
			vinpt3d.push_back(vpt3d[i]);		// 3d cooridnate
			vinkpt.push_back(vnewtrack[inlier_vnewtrack_number[i]]->kpt);		// keypoint
			++numinliers;
		}
	}

	if (float(numinliers) / float(vleft.size()) < params.GOODINIT){
		LOGOUT("Not enough inliers for initialization\n");
		return false;
	}
	

	// add points to keyframe and map
	cv::Mat vindesc;
	if (vinkpt.size() != 0){
		detector.Describe(G_img, vinkpt, vindesc);
	}

	std::vector<int> vid;		// ID of points
	mdata.map.UpdateLastKeyframe(vinpt3d, vinkpt, vindesc, vid);

	// set point ID to new tracks
	int counter = 0;
	for (int i = 0, iend = int(vinlier.size()); i < iend; ++i){
		if (vinlier[i]){
			vnewtrack[inlier_vnewtrack_number[i]]->ptID = vid[counter];
			++counter;
		}
	}

	return true;
}

void online_sfm::initialize(){

	startInit();

	if (makeMap_init()){
		setKeyframe();
		// change state   
		mState = STATE::TAM;
		mText = "Tracking and Mapping";
	}
	else{
		mText = "Initialize failed.";
		reset();
	}
}

void online_sfm::relocalize()
{
	if (mdata.vKpt.size() == 0){
		return;
	}
	else if (matchKeyframe()){
		if (mdata.havescale){
			mText = "Relocalized";
		}
		else{
			mText = "Tracking and Mapping";
		}

		LOGOUT("Relocalized\n");
		mState = STATE::TAM;
	}
}

void online_sfm::drawView(cv::Mat &img)
{
	const sKeyframe& kf = mdata.map.GetNearestKeyframe(mPose);

	cv::Mat mono = cv::Mat(img.size(), CV_8U);
	cv::Canny(kf.img, mono, 10, 100);

	std::vector<cv::Mat> color(3, mono);

	cv::Mat cimg;
	cv::merge(color, cimg);

	double alpha = 0.5;
	double beta = (1.0 - alpha);
	addWeighted(img.clone(), alpha, cimg, beta, 0.0, img);
}


/*void online_sfm::localBA()
{
	while (1){
		STATE mainState = mState;

		if (mainState == STATE::TAM){
			// get data from map
			bool copied = mdata.map.CopytoBA(mdata.baData);
			std::vector<sKeyframe> &vkf = mdata.baData.vkeyframe;
			int numkeyframes = int(vkf.size());

			if (copied && numkeyframes > 2){

				mBAMutex.lock();
				mDoingBA = true;
				mBAMutex.unlock();

				// check visibility of mapped points in each keyframe
				std::vector<cv::Point3f> &vpt3d = mdata.baData.vpt3d;
				std::vector<int> checkvis(vpt3d.size(), 0);

				for (int i = 0, iend = numkeyframes; i < iend; ++i){
					sKeyframe &kf = vkf[i];
					for (int j = 0, jend = int(kf.vptID.size()); j < jend; ++j){
						++checkvis[kf.vptID[j]];
					}
				}

				// select visible mapped points
				std::vector<cv::Point3f> vusedpt3d;
				std::vector<int> vvisibleID;

				for (int i = 0, iend = int(checkvis.size()); i < iend; ++i){
					if (checkvis[i] > 1){	// should be visible in more than two views
						int num = int(vusedpt3d.size());
						checkvis[i] = num;
						vvisibleID.push_back(i);
						vusedpt3d.push_back(vpt3d[i]);
					}
					else{
						checkvis[i] = -1;
					}
				}

				// set data for cvsba
				std::vector< std::vector<cv::Point2f> > imagePoints(numkeyframes);
				std::vector< std::vector<int> > visibility(numkeyframes);
				std::vector<cv::Mat> cameraMatrix(numkeyframes);
				std::vector<cv::Mat> R(numkeyframes);
				std::vector<cv::Mat> T(numkeyframes);
				std::vector<cv::Mat> distCoeffs(numkeyframes);

				for (int i = 0; i < numkeyframes; ++i){

					std::vector<cv::Point2f> points(vusedpt3d.size());
					std::vector<int> vis(vusedpt3d.size(), 0);

					sKeyframe &kf = vkf[i];
					for (int j = 0, jend = int(kf.vptID.size()); j < jend; ++j){
						int id = checkvis[kf.vptID[j]];

						if (id != -1){
							points[id] = kf.vpt[j];
							vis[id] = 1;
						}
					}

					imagePoints[i] = points;
					visibility[i] = vis;
					cameraMatrix[i] = mdata.A;
					distCoeffs[i] = mdata.D;
					kf.pose.rvec.copyTo(R[i]);
					kf.pose.tvec.copyTo(T[i]);
				}

				LOGOUT("BA started with %d points %d frames\n", vpt3d.size(), numkeyframes);
				double val = mBA.run(vusedpt3d, imagePoints, visibility, cameraMatrix, R, T, distCoeffs);

				if (val < 0){
					LOGOUT("BA failed\n");
				}
				else{
					LOGOUT("Initial error = \t %f\n", mBA.getInitialReprjError());
					LOGOUT("Final error = \t %f\n", mBA.getFinalReprjError());

					for (int i = 0; i < numkeyframes; ++i){
						sKeyframe &kf = vkf[i];

						R[i].copyTo(kf.pose.rvec);
						T[i].copyTo(kf.pose.tvec);
					}

					for (int i = 0, iend = int(vvisibleID.size()); i < iend; ++i){
						int id = vvisibleID[i];
						vpt3d[id] = vusedpt3d[i];
					}

					mdata.baData.vvisibleID = vvisibleID;
					mdata.map.CopyfromBA(mdata.baData);
				}

				mBAMutex.lock();
				mDoingBA = false;
				mBAMutex.unlock();
			}
		}
		else if (mainState == STATE::CLOSE){
			break;
		}
	}
}*/
void online_sfm::run(std::vector<cv::Mat> img){
	img[0].copyTo(G_img);
	img[1].copyTo(right_cam_g);

	mdata.vKpt.clear();
	detector.Detect(G_img, mdata.vKpt);

	switch (mState){
	case STATE::INIT:
		initialize();
		break;
	case STATE::TAM:
		trackAndMap();
		break;
	case STATE::RELOCAL:
		relocalize();
		break;
	default:
		break;
	}

	G_img.copyTo(mdata.previmg);

	// compute camera location in world coordinate system
	cv::Mat R;
	cv::Rodrigues(mPose.rvec, R);
	R = R.inv();
	R.copyTo(left_cam_pose.R);
	cv::Rodrigues(R, left_cam_pose.rvec);
	cv::Mat tvec;
    tvec = -R * mPose.tvec;
	tvec.copyTo(left_cam_pose.tvec);
}

bool online_sfm::operation(const int key)
{
	if (key == ' '){		// change state
		changeState();
	}
	else if (key == 'r'){	// reset
		reset();
	}
	else if (key == 0x1b){	// exit
		return true;
	}

	return false;
}

void online_sfm::draw(cv::Mat &img)
{
	// each process
	switch (mState){
	case STATE::TAM:
		drawTrack(img);
		drawMap(img);
		break;
	case STATE::RELOCAL:
		drawView(img);
		break;
	default:
		break;
	}

	drawProcess(img);
}

void online_sfm::finish(){
	mState = STATE::CLOSE;
	// BA.join();
}