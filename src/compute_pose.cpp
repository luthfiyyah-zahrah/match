#include "match/compute_pose.h"

extern Params params;

compute_pose::compute_pose(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length)
{
    DoingStereo = false;
    init_params(A, D, img_size, baseline_length);
}

compute_pose::~compute_pose()
{
    //dtor
}

void compute_pose::init_params(cv::Mat A, cv::Mat D, cv::Size img_size, double baseline_length)
{
	// set camera parameters to ATAM
	A.copyTo(mdata.A);					// camera paramters
	D.copyTo(mdata.D);					// distortion parameters
	mdata.focal = mdata.A.at<double>(0,0);	// focal length

	//using_only_initialize
	right_Pose.rvec.setTo(0);
	right_Pose.tvec.setTo(0);
	right_Pose.tvec.at<double>(0, 0) = baseline_length;

	//set initial pose
	mPose.rvec.setTo(0);
	mPose.tvec.setTo(0);
	left_cam_pose.rvec.setTo(0);
	left_cam_pose.tvec.setTo(0);

	// image
	G_img = cv::Mat(img_size, CV_8UC1);
	right_cam_g = cv::Mat(img_size, CV_8UC1);

	// initialize keypoint detector
	// detector.Init(params.MAXPTS, params.LEVEL);
}

void compute_pose::compute()
{
	// pose estimation
	std::vector<cv::Point2f> vpt2d;
	std::vector<cv::Point3f> vpt3d;

	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID != NOID){
			vpt2d.push_back(it->vpt.back());
			vpt3d.push_back(mdata.map.GetPoint(it->ptID));
		}
	}

	cv::solvePnP(vpt3d, vpt2d, mdata.A, mdata.D, mPose.rvec, mPose.tvec, true);

	// check reprojection error and discard points if error is large
	std::vector< cv::Point2f > vrepropt;
	cv::projectPoints(vpt3d, mPose.rvec, mPose.tvec, mdata.A, mdata.D, vrepropt);

	int numall = 0;
	int numdiscard = 0;

	for (std::list<sTrack>::iterator it = mdata.vtrack.begin(), itend = mdata.vtrack.end();	it != itend; ++it) {
		if (it->ptID != NOID){

			double dist = cv::norm(vrepropt[numall] - vpt2d[numall]);

			if (dist > params.PROJERR){
				it->ptID = DISCARD;
				++numdiscard;
			}
			++numall;
		}
	}

	mdata.clearTrack(DISCARD);
	LOGOUT("Discarded:%d used:%d at %s\n", numdiscard, numall - numdiscard, __FUNCTION__);

	mdata.quality = double(numall - numdiscard) / double(numall);		// inlier ratio

	// if (numall - numdiscard > params.MINPTS){
	// 	return true;
	// }
	// else{
	// 	return false;
	// }
}

void compute_pose::run(std::vector<cv::Mat> img){
	cv::ORB detector;
	// get image
	img[0].copyTo(G_img);
	img[1].copyTo(right_cam_g);
	//detect ORB
	mdata.vKpt.clear();
	detector.detect(G_img, mdata.vKpt);

	compute();

	G_img.copyTo(mdata.previmg);

	// compute camera location in world coordinate system
	cv::Mat R;
	cv::Rodrigues(mPose.rvec, R);
	R = R.inv();
	R.copyTo(left_cam_pose.R);
	cv::Rodrigues(R, left_cam_pose.rvec);
	cv::Mat tvec;
    tvec = -R * mPose.tvec;
	tvec.copyTo(left_cam_pose.tvec);
}
