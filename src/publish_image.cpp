#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sstream> // for converting the command line parameter to integer
 
int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_publisher");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);

  image_transport::Publisher publeft = it.advertise("left/image_raw", 1);
  image_transport::Publisher pubright = it.advertise("right/image_raw", 1);

  cv::Mat image1 = cv::imread("/home/zahrah/left03.jpg", CV_LOAD_IMAGE_COLOR);
  sensor_msgs::ImagePtr msg1 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image1).toImageMsg();

  
  cv::Mat image2 = cv::imread("/home/zahrah/right03.jpg", CV_LOAD_IMAGE_COLOR);
  cv::waitKey(30);
  sensor_msgs::ImagePtr msg2 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image2).toImageMsg();

  ros::Rate loop_rate(30);
  while (nh.ok()) {
  publeft.publish(msg1);
  pubright.publish(msg2);
  ros::spinOnce();
  loop_rate.sleep();
  }
}