#include "match/Timer.h"
#include <iostream>

int Timer::Pop(const bool flag)
{
	int duration;

	std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
	std::chrono::duration<double> sec = end - mTasks.back().stime;

	duration = int(std::chrono::duration_cast<std::chrono::milliseconds>(sec).count());

	if (flag){
		std::cout << mTasks.back().name.c_str() << ": " << duration << "ms" << std::endl;
	}

	mTasks.pop_back();

	return duration;
}

void Timer::Push(const std::string &name)
{
	sTask tmp;
	tmp.name = name;

	tmp.stime = std::chrono::system_clock::now();

	mTasks.push_back(tmp);
}
