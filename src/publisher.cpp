#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <time.h>
void waitFor (unsigned int secs) {
    unsigned int retTime = time(0) + secs;   // Get finishing time.
    while (time(0) < retTime);               // Loop until it arrives.
}
 int main(int argc, char** argv)
 {
   ros::init(argc, argv, "image_publisher");
   ros::NodeHandle nh;
   image_transport::ImageTransport it(nh);
   image_transport::Publisher pub = it.advertise("cam/image", 1);
 
   ros::Rate loop_rate(1);
   int loop=1;
   cv::Mat image;
   while (nh.ok()) {
      if(loop==1){
         image = cv::imread("/home/zahrah/bear.jpg", CV_LOAD_IMAGE_COLOR);
      }
      if(loop==2){
         image = cv::imread("/home/zahrah/bear2.jpg", CV_LOAD_IMAGE_COLOR);
      }
      if(loop==3){
         image = cv::imread("/home/zahrah/bear3.jpg", CV_LOAD_IMAGE_COLOR);
      }
      if(loop<4){
        loop+=1;
      }else{
        break;
      }
      printf("loop %d",loop);
      
  //   cv::waitKey(30);
     sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
     pub.publish(msg);
     ros::spinOnce();
     loop_rate.sleep();
     waitFor (10);
   }
 }