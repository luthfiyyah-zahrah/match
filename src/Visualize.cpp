#include "match/Visualize.h"

Visualize::Visualize()
{
	draw_path = false;
	update = false;
	end_flag = false;
	vis = std::thread(&Visualize::visualize_multi, this);
}
Visualize::~Visualize()
{
}

void Visualize::drawing_path_area(){
	line_name.clear();
	for (int i = 0; i < 8; ++i){
		line_name.push_back(std::to_string(i));
		viewer->removeShape(line_name[i]);
	}

	viewer->addLine(Path_area.left_left_s, Path_area.left_right_s, line_name[0]);
	viewer->addLine(Path_area.left_left_s, Path_area.left_left_g, line_name[1]);
	viewer->addLine(Path_area.left_left_g, Path_area.left_right_g, line_name[2]);
	viewer->addLine(Path_area.left_right_s, Path_area.left_right_g, line_name[3]);

	viewer->addLine(Path_area.right_left_s, Path_area.right_right_s, line_name[4]);
	viewer->addLine(Path_area.right_left_s, Path_area.right_left_g, line_name[5]);
	viewer->addLine(Path_area.right_left_g, Path_area.right_right_g, line_name[6]);
	viewer->addLine(Path_area.right_right_s, Path_area.right_right_g, line_name[7]);

}

void Visualize::convert(cv::Mat r_vec, cv::Mat t_vec, pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud)
{
	r_vector = r_vec;
	t_vector = t_vec;
	mycloud = point_cloud->makeShared();
}

void Visualize::visualize_multi(){
	viewer = cv::Ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->initCameraParameters();
	double view_z_position = -1500.0;////カメラの後方？mmのところから見る．

	while (!viewer->wasStopped()){
		if (end_flag)
			break;

		viewer->spinOnce(3);	
		visMutex.lock();
		if (update){
			cv::Mat tmp_R;
			cv::Rodrigues(r_vector, tmp_R);
			cv::Mat view_point_position = (cv::Mat_<double>(3, 1) << tmp_R.at<double>(0, 2) * view_z_position + t_vector.at<double>(0, 0),
				tmp_R.at<double>(1, 2) * view_z_position + t_vector.at<double>(1, 0), 
				tmp_R.at<double>(2, 2) * view_z_position + t_vector.at<double>(2, 0));
			
			viewer->setCameraPosition(view_point_position.at<double>(0, 0), view_point_position.at<double>(1, 0), view_point_position.at<double>(2, 0),
				t_vector.at<double>(0, 0), t_vector.at<double>(1, 0), t_vector.at<double>(2, 0),
				0, -1, 0);
			//setcamerapositions...pos_x, pos_y, pos_z --> Set position of camera, view_x, view_y, view_z --> Set point of interest/focus for your camera, up_x, up_y, up_z --> Orientation of cloud in relation to your camera... n.b. set up_y = -1 to flip your cloud right way up. 
	
			
			Eigen::Affine3f tt;
			Eigen::Translation3f t_Vec(t_vector.at<double>(0, 0), t_vector.at<double>(1, 0), t_vector.at<double>(2, 0));
			Eigen::AngleAxisf r_VecX(r_vector.at<double>(0, 0), Eigen::Vector3f::UnitX());
			Eigen::AngleAxisf r_VecY(r_vector.at<double>(1, 0), Eigen::Vector3f::UnitY());
			Eigen::AngleAxisf r_VecZ(r_vector.at<double>(2, 0), Eigen::Vector3f::UnitZ());
			tt = t_Vec * r_VecX * r_VecY * r_VecZ;

			if (!viewer->updatePointCloud(mycloud)){
				viewer->addPointCloud(mycloud);
			}
			if (!viewer->updateCoordinateSystemPose("cam_pos", tt)){
				viewer->addCoordinateSystem(200.0, tt, "cam_pos");
			}

			if (path){
				//////グリッドの描画がめちゃくちゃ重い....OpenGLとか使うといいかもしれません．（使ったことはないのでよくわかりませんが）
				drawing_path_area();
				draw_path = false;
				path = false;
			}

			update = false;
		}
		visMutex.unlock();
	}
}

void Visualize::run(cv::Mat r_vec, cv::Mat t_vec, pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud){
	visMutex.lock();
	update = true;
	path = draw_path;
	convert(r_vec, t_vec, point_cloud);
	visMutex.unlock();
}

void Visualize::finish()
{
	end_flag = true;
	vis.join();
}