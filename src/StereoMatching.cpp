#include "match/StereoMatching.h"


StereoMatching::StereoMatching(cv::Mat Q, cv::Size imgsize)
{
	point_cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>);
	// set_params();
	// cv::StereoSGBM sgbm;
	// // cv::StereoSGBM::StereoSGBM(16, ndisp * 16, sad_blocksize);
	// // sgbm = cv::StereoSGBM::create(16, ndisp * 16, sad_blocksize);
	// //内部でデフォルトで設定されているが，何をやっているのか意識するため明示している．
	// sgbm.preFilterCap=4;
	// sgbm.uniquenessRatio=1;
	// sgbm.speckleWindowSize=100;
	// sgbm.speckleRange=5;
	// sgbm.disp12MaxDiff=1;
	// sgbm.minDisparity = -128;
	// sgbm.numberOfDisparities = 256;
	// // sgbm.mode(cv::StereoSGBM::MODE_HH);
	// sgbm.fullDP =true;
	// sgbm.P1=8 * 1 * sad_blocksize*sad_blocksize;
	// sgbm.P2=32 * 1 * sad_blocksize*sad_blocksize;
	tree.reset(new pcl::search::KdTree<pcl::PointXYZRGB>());
	plane_coeffs.reset(new pcl::ModelCoefficients);

	Q.copyTo(repro_Q);
	disp_vis = cv::Mat::zeros(imgsize, CV_8U);


}
StereoMatching::~StereoMatching()
{
}

void StereoMatching::downsampling()
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr down_sampling(new pcl::PointCloud<pcl::PointXYZRGB>);
	// Create the filtering object
	pcl::VoxelGrid<pcl::PointXYZRGB> sor;
	sor.setInputCloud(point_cloud);
	sor.setLeafSize(15.f, 15.f, 15.f);
	sor.filter(*down_sampling);

	point_cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>(*down_sampling));
}



void StereoMatching::outlier_removal(){
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr outlier_remove(new pcl::PointCloud<pcl::PointXYZRGB>);
	// Create the filtering object
	pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
	sor.setInputCloud(point_cloud);
	sor.setMeanK(50);
	sor.setStddevMulThresh(1.0);
	sor.filter(*outlier_remove);

	point_cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>(*outlier_remove));
}

void StereoMatching::detect_plane()
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr detect_plane(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;

	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType(pcl::SACMODEL_PLANE);//検出するモデルのタイプを指定
	seg.setMethodType(pcl::SAC_RANSAC);//検出に使用する方法を指定
	seg.setDistanceThreshold(10.0);//

	seg.setInputCloud(point_cloud);
	seg.segment(*inliers, *plane_coeffs);
}

void StereoMatching::create_estimated_path_area(cv::Mat r_vector, cv::Mat t_vector){

	cv::Mat reference_point(3, 1, CV_64F);//推定された地面に対しての現在のカメラ位置の正射影

	double tmp_Val1 = -plane_coeffs->values[0] * t_vector.at<double>(0, 0) - plane_coeffs->values[1] * t_vector.at<double>(1, 0) - plane_coeffs->values[2] * t_vector.at<double>(2, 0) - plane_coeffs->values[3];
	double tmp_Val2 = plane_coeffs->values[0] * plane_coeffs->values[0] + plane_coeffs->values[1] * plane_coeffs->values[1] + plane_coeffs->values[2] * plane_coeffs->values[2];
	reference_point.at<double>(0, 0) = plane_coeffs->values[0] * (tmp_Val1 / tmp_Val2) + t_vector.at<double>(0, 0);
	reference_point.at<double>(1, 0) = plane_coeffs->values[1] * (tmp_Val1 / tmp_Val2) + t_vector.at<double>(1, 0);
	reference_point.at<double>(2, 0) = plane_coeffs->values[2] * (tmp_Val1 / tmp_Val2) + t_vector.at<double>(2, 0);

	cv::Mat tmp_R;
	cv::Rodrigues(r_vector, tmp_R);
	cv::Mat tmp_reference_x(3, 1, CV_64F), tmp_reference_z(3, 1, CV_64F);

	tmp_reference_x.at<double>(0, 0) = tmp_R.at<double>(0, 0) * 100 + t_vector.at<double>(0, 0);
	tmp_reference_x.at<double>(1, 0) = tmp_R.at<double>(1, 0) * 100 + t_vector.at<double>(1, 0);
	tmp_reference_x.at<double>(2, 0) = tmp_R.at<double>(2, 0) * 100 + t_vector.at<double>(2, 0);

	tmp_reference_z.at<double>(0, 0) = tmp_R.at<double>(0, 2) * 100 + t_vector.at<double>(0, 0);
	tmp_reference_z.at<double>(1, 0) = tmp_R.at<double>(1, 2) * 100 + t_vector.at<double>(1, 0);
	tmp_reference_z.at<double>(2, 0) = tmp_R.at<double>(2, 2) * 100 + t_vector.at<double>(2, 0);

	cv::Mat reference_x(3, 1, CV_64F), reference_y(3, 1, CV_64F), reference_z(3, 1, CV_64F);
	tmp_Val1 = -plane_coeffs->values[0] * tmp_reference_x.at<double>(0, 0) - plane_coeffs->values[1] * tmp_reference_x.at<double>(1, 0) - plane_coeffs->values[2] * tmp_reference_x.at<double>(2, 0) - plane_coeffs->values[3];
	tmp_Val2 = plane_coeffs->values[0] * plane_coeffs->values[0] + plane_coeffs->values[1] * plane_coeffs->values[1] + plane_coeffs->values[2] * plane_coeffs->values[2];

	reference_x.at<double>(0, 0) = plane_coeffs->values[0] * (tmp_Val1 / tmp_Val2) + tmp_reference_x.at<double>(0, 0);
	reference_x.at<double>(1, 0) = plane_coeffs->values[1] * (tmp_Val1 / tmp_Val2) + tmp_reference_x.at<double>(1, 0);
	reference_x.at<double>(2, 0) = plane_coeffs->values[2] * (tmp_Val1 / tmp_Val2) + tmp_reference_x.at<double>(2, 0);

	cv::Mat tmp_norm = reference_x - reference_point;
	double x_norm = cv::norm(tmp_norm);

	cv::Mat x_unit  = tmp_norm / x_norm;
	
	double left_outside_pos = 198.5;
	double right_outside_pos = 298.5;

	cv::Mat left_wheel_pos_left = reference_point + left_outside_pos * (-x_unit);////数字は実測距離
	cv::Mat left_wheel_pos_right = reference_point + (left_outside_pos - 85.0) * (-x_unit);
	cv::Mat right_wheel_pos_left = reference_point + (right_outside_pos - 85.0) * x_unit;
	cv::Mat right_wheel_pos_right = reference_point + right_outside_pos * x_unit;

	Path_area.left_left_s.x = left_wheel_pos_left.at<double>(0, 0); Path_area.left_left_s.y = left_wheel_pos_left.at<double>(1, 0); Path_area.left_left_s.z = left_wheel_pos_left.at<double>(2, 0);
	Path_area.left_right_s.x = left_wheel_pos_right.at<double>(0, 0); Path_area.left_right_s.y = left_wheel_pos_right.at<double>(1, 0); Path_area.left_right_s.z = left_wheel_pos_right.at<double>(2, 0);
	Path_area.right_left_s.x = right_wheel_pos_left.at<double>(0, 0); Path_area.right_left_s.y = right_wheel_pos_left.at<double>(1, 0); Path_area.right_left_s.z = right_wheel_pos_left.at<double>(2, 0);
	Path_area.right_right_s.x = right_wheel_pos_right.at<double>(0, 0); Path_area.right_right_s.y = right_wheel_pos_right.at<double>(1, 0); Path_area.right_right_s.z = right_wheel_pos_right.at<double>(2, 0);

	tmp_Val1 = -plane_coeffs->values[0] * tmp_reference_z.at<double>(0, 0) - plane_coeffs->values[1] * tmp_reference_z.at<double>(1, 0) - plane_coeffs->values[2] * tmp_reference_z.at<double>(2, 0) - plane_coeffs->values[3];
	tmp_Val2 = plane_coeffs->values[0] * plane_coeffs->values[0] + plane_coeffs->values[1] * plane_coeffs->values[1] + plane_coeffs->values[2] * plane_coeffs->values[2];

	reference_z.at<double>(0, 0) = plane_coeffs->values[0] * (tmp_Val1 / tmp_Val2) + tmp_reference_z.at<double>(0, 0);
	reference_z.at<double>(1, 0) = plane_coeffs->values[1] * (tmp_Val1 / tmp_Val2) + tmp_reference_z.at<double>(1, 0);
	reference_z.at<double>(2, 0) = plane_coeffs->values[2] * (tmp_Val1 / tmp_Val2) + tmp_reference_z.at<double>(2, 0);

	tmp_norm = reference_z - reference_point;
	double z_norm = cv::norm(tmp_norm);

	cv::Mat z_unit = tmp_norm / z_norm;
	double estimate_length = 1000.0;

	cv::Mat estimate_path_left_left = left_wheel_pos_left + estimate_length * z_unit;
	cv::Mat estimate_path_left_right = left_wheel_pos_right + estimate_length * z_unit;
	cv::Mat estimate_path_right_left = right_wheel_pos_left + estimate_length * z_unit;
	cv::Mat estimate_path_right_right = right_wheel_pos_right + estimate_length * z_unit;

	Path_area.left_left_g.x = estimate_path_left_left.at<double>(0, 0); Path_area.left_left_g.y = estimate_path_left_left.at<double>(1, 0); Path_area.left_left_g.z = estimate_path_left_left.at<double>(2, 0);
	Path_area.left_right_g.x = estimate_path_left_right.at<double>(0, 0); Path_area.left_right_g.y = estimate_path_left_right.at<double>(1, 0); Path_area.left_right_g.z = estimate_path_left_right.at<double>(2, 0);
	Path_area.right_left_g.x = estimate_path_right_left.at<double>(0, 0); Path_area.right_left_g.y = estimate_path_right_left.at<double>(1, 0); Path_area.right_left_g.z = estimate_path_right_left.at<double>(2, 0);
	Path_area.right_right_g.x = estimate_path_right_right.at<double>(0, 0); Path_area.right_right_g.y = estimate_path_right_right.at<double>(1, 0); Path_area.right_right_g.z = estimate_path_right_right.at<double>(2, 0);

	double theta = acos(plane_coeffs->values[1] / sqrt(pow(plane_coeffs->values[0], 2.0) + pow(plane_coeffs->values[1], 2.0) + pow(plane_coeffs->values[2], 2.0)));
	if (theta < 90){
		reference_y.at<double>(0, 0) = -100 * plane_coeffs->values[0];
		reference_y.at<double>(1, 0) = -100 * plane_coeffs->values[1];
		reference_y.at<double>(2, 0) = -100 * plane_coeffs->values[2];
	}
	else{
		reference_y.at<double>(0, 0) = 100 * plane_coeffs->values[0];
		reference_y.at<double>(1, 0) = 100 * plane_coeffs->values[1];
		reference_y.at<double>(2, 0) = 100 * plane_coeffs->values[2];
	}

	double y_norm = cv::norm(reference_y);

	reference_y += reference_point;

	cv::Mat o_point = (cv::Mat_<double>(3, 4) << 0, x_norm, 0, 0,
		0, 0, y_norm, 0,
		0, 0, 0, z_norm);

	cv::Mat transformed_P(4, 4, reference_point.type(), cv::Scalar(1)), original_P(4, 4, reference_point.type(), cv::Scalar(1));
	reference_point.copyTo(transformed_P(cv::Rect(0, 0, 1, 3)));
	reference_x.copyTo(transformed_P(cv::Rect(1, 0, 1, 3)));
	reference_y.copyTo(transformed_P(cv::Rect(2, 0, 1, 3)));
	reference_z.copyTo(transformed_P(cv::Rect(3, 0, 1, 3)));

	o_point.copyTo(original_P(cv::Rect(0, 0, 4, 3)));
	cv::Mat ground_coordinate;
	ground_coordinate = transformed_P * original_P.inv();
	ground_R = ground_coordinate(cv::Rect(0, 0, 3, 3));
	ground_t = ground_coordinate(cv::Rect(3, 0, 1, 3));

}

void StereoMatching::compute_height(){
	std::cout<<"compute1"<<std::endl;
	cv::Mat transform_from_ground(4, 4, ground_R.type(), cv::Scalar(0));
	ground_R.copyTo(transform_from_ground(cv::Rect(0, 0, 3, 3)));
	ground_t.copyTo(transform_from_ground(cv::Rect(3, 0, 1, 3)));
	transform_from_ground.at<double>(3, 3) = 1.0;

	transform_from_ground = transform_from_ground.inv();

	cv::Mat transform_R(3, 3, ground_R.type()), transform_t(3, 1, ground_R.type());
	transform_R = transform_from_ground(cv::Rect(0, 0, 3, 3));
	transform_t = transform_from_ground(cv::Rect(3, 0, 1, 3));


	double left_lim_left, left_lim_right, right_lim_left, right_lim_right, path_area_lim;
	left_lim_left = transform_R.at<double>(0, 0) * Path_area.left_left_s.x + transform_R.at<double>(0, 1) * Path_area.left_left_s.y + transform_R.at<double>(0, 2) * Path_area.left_left_s.z + transform_t.at<double>(0, 0);
	left_lim_right = transform_R.at<double>(0, 0) * Path_area.left_right_s.x + transform_R.at<double>(0, 1) * Path_area.left_right_s.y + transform_R.at<double>(0, 2) * Path_area.left_right_s.z + transform_t.at<double>(0, 0);
	right_lim_left = transform_R.at<double>(0, 0) * Path_area.right_left_s.x + transform_R.at<double>(0, 1) * Path_area.right_left_s.y + transform_R.at<double>(0, 2) * Path_area.right_left_s.z + transform_t.at<double>(0, 0);
	right_lim_right = transform_R.at<double>(0, 0) * Path_area.right_right_s.x + transform_R.at<double>(0, 1) * Path_area.right_right_s.y + transform_R.at<double>(0, 2) * Path_area.right_right_s.z + transform_t.at<double>(0, 0);
	path_area_lim = transform_R.at<double>(2, 0) * Path_area.left_left_g.x + transform_R.at<double>(2, 1) * Path_area.left_left_g.y + transform_R.at<double>(2, 2) * Path_area.left_left_g.z + transform_t.at<double>(2, 0);

	_estimate_area_info estimate_area;

	bool inlier;
	for (pcl::PointCloud<pcl::PointXYZRGB>::iterator it = point_cloud->begin(), it_end = point_cloud->end(); it != it_end; ++it){
		inlier = false;
		double x = transform_R.at<double>(0, 0) * it->x + transform_R.at<double>(0, 1) * it->y + transform_R.at<double>(0, 2) * it->z + transform_t.at<double>(0, 0);
		double z = transform_R.at<double>(2, 0) * it->x + transform_R.at<double>(2, 1) * it->y + transform_R.at<double>(2, 2) * it->z + transform_t.at<double>(2, 0);
		double height = transform_R.at<double>(1, 0) * it->x + transform_R.at<double>(1, 1) * it->y + transform_R.at<double>(1, 2) * it->z + transform_t.at<double>(1, 0);

		if (x > left_lim_left && x < left_lim_right && z < path_area_lim){
			inlier = true;
		}
		else if (x > right_lim_left && x < right_lim_right && z < path_area_lim){
			inlier = true;
		}
		if (inlier){
			estimate_area.point_num.push_back(it - point_cloud->begin());
			estimate_area.height.push_back(-height);
		}
	}
	std::cout<<"compute2"<<std::endl;
	////高さによって色を塗る...計測した高さがグリッドの配列の中に入っているかの確認用
	cv::Mat img1(1, 200, CV_8UC3);
	cv::Mat img2(1, 200, CV_8UC3);
	for (int y = 0; y<img1.rows; y++) {
		for (int x = 0; x<img1.cols; x++) {
			//結果は同じ
			img1.at<cv::Vec3b>(y, x)[0] = (img1.cols - x) / 4.0;
			img1.at<cv::Vec3b>(y, x)[1] = 255;
			img1.at<cv::Vec3b>(y, x)[2] = 255;
		}
	}
	
	cv::cvtColor(img1, img2, cv::COLOR_HSV2BGR_FULL);
	std::cout<<"compute3"<<std::endl;
	for (int j = 0; j < estimate_area.height.size(); ++j){
		pcl::PointCloud<pcl::PointXYZRGB>::iterator it = point_cloud->begin();
		it += estimate_area.point_num[j];
		
		cv::Vec3b bgr = img2.at<cv::Vec3b>(0, (int)100);
		it->b = bgr[0];
		it->g = bgr[1];
		it->r = bgr[2];
	}
	height = detect_highest_point(estimate_area);
	std::cout<<"compute5"<<std::endl;
}

double StereoMatching::detect_highest_point(_estimate_area_info estimate_area){
	std::sort(estimate_area.height.begin(), estimate_area.height.end(), std::greater<double>());
	std::vector<double> detect_height(10);
	for (int i = 0; i < 10; ++i)
		detect_height[9-i] = estimate_area.height[i];

	return smirnov_grubbs(detect_height);
}

double StereoMatching::smirnov_grubbs(std::vector<double> height_candidate){
	
	for (size_t i = 0; i < 10; i++){
		double tmp_mean = 0, tmp_var = 0;
		for (size_t j = 0; j < height_candidate.size(); j++){
			tmp_mean += height_candidate[j];
		}
		tmp_mean /= (double)height_candidate.size();
		for (size_t j = 0; j < height_candidate.size(); j++){
			tmp_var += (height_candidate[j] - tmp_mean)*(height_candidate[j] - tmp_mean);
		}
		tmp_var /= ((double)height_candidate.size() - 1.0);
		double Smirnov;
		Smirnov = abs(height_candidate.back() - tmp_mean) / sqrt(tmp_var);
		if (Smirnov >= 2.176){
			height_candidate.pop_back();
			std::cout << "filterd out point by Smirnov‐Grubbs" << std::endl;
		}
		else
			break;
	}
	return height_candidate.back();
}

void StereoMatching::createPointCloud(cv::Mat img_orig, cv::Mat R, cv::Mat tvec){
	std::cout<<"create point cloud1"<<std::endl;
	const double max_z = 1.0e4;
	point_cloud->clear();
	for (int y = 0; y < xyz.rows; y++)
	{
		for (int x = 0; x < xyz.cols; x++)
		{ 
			cv::Vec3f point = xyz.at<cv::Vec3f>(y, x);
			cv::Vec3b color = img_orig.at<cv::Vec3b>(y, x);
			if (fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z) continue;
			pcl::PointXYZRGB pc;
			pc.x = R.at<double>(0, 0) * point[0] + R.at<double>(0, 1) * point[1] + R.at<double>(0, 2) * point[2] + tvec.at<double>(0, 0);
			pc.y = R.at<double>(1, 0) * point[0] + R.at<double>(1, 1) * point[1] + R.at<double>(1, 2) * point[2] + tvec.at<double>(1, 0);
			pc.z = R.at<double>(2, 0) * point[0] + R.at<double>(2, 1) * point[1] + R.at<double>(2, 2) * point[2] + tvec.at<double>(2, 0);
			pc.b = color[0];
			pc.g = color[1];
			pc.r = color[2];
			point_cloud->points.push_back(pc);
		}

	}
	point_cloud->width = (int)point_cloud->points.size();
	point_cloud->height = 1;

	std::cout<<point_cloud->width<<std::endl;
	downsampling();
	outlier_removal();
	std::cout<<"create point cloud3"<<std::endl;
	detect_plane();

	cv::Mat r;
	cv::Rodrigues(R, r);
	create_estimated_path_area(r, tvec);
std::cout<<"create point cloud4"<<std::endl;
	compute_height();
std::cout<<"create point cloud5"<<std::endl;
}

void StereoMatching::run(std::vector<cv::Mat> img_g,cv::Mat img_o)
{
	
	set_params();
	std::cout<<"here param"<<std::endl;
	cv::StereoSGBM sgbm;
	std::cout<<"here deklarasi"<<std::endl;
	sgbm.preFilterCap=31;
	sgbm.uniquenessRatio=10;
	sgbm.speckleWindowSize=200;
	sgbm.speckleRange=1;
	sgbm.disp12MaxDiff=1;
	sgbm.minDisparity = 0;
	sgbm.numberOfDisparities = 256;
	// sgbm.mode(cv::StereoSGBM::MODE_HH);
	sgbm.fullDP =true;
	sgbm.P1=8 * 1 * sad_blocksize*sad_blocksize;
	sgbm.P2=32 * 1 * sad_blocksize*sad_blocksize;
	std::cout<<"here deklarasi2"<<std::endl;
	sgbm(img_g[0], img_g[1], disp);
	std::cout<<"here disparity"<<std::endl;
	cv::reprojectImageTo3D(disp, xyz, repro_Q, true);
	std::cout<<"here reprojek"<<std::endl;
	normalize(disp, disp_vis, 0, 255, CV_MINMAX, CV_8U);
	std::cout<<"here finshing"<<std::endl;
}

void StereoMatching::set_params()
{
	ndisp = 4;
	sad_blocksize = 3;
	// STEREO_HH = 2;
	grid_size = 50.0;//mm
	map_size_x = 3000.0;//mm
	map_size_y = 3600.0;//mm
}
