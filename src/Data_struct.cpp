#include "match/Data_struct.h"

Params params;

sPose::sPose()
{
	rvec = cv::Mat::eye(cvSize(1, 3), CV_64F);
	tvec = cv::Mat::eye(cvSize(1, 3), CV_64F);
	R = cv::Mat::eye(cvSize(3, 3), CV_64F);
}

sPose::sPose(const sPose& r)
{
	r.rvec.copyTo(rvec);
	r.tvec.copyTo(tvec);
	r.R.copyTo(R);
}

sPose& sPose::operator=(const sPose& r)
{
	r.rvec.copyTo(rvec);
	r.tvec.copyTo(tvec);
	r.R.copyTo(R);
	return *this;
}

void sPose::print() const
{
	std::cout << rvec.t() << std::endl;
	std::cout << tvec.t() << std::endl;
	std::cout << R << std::endl;
}

sTrack::sTrack()
{
	ptID = NOID;
}

void sKeyframe::clear()
{
	vkptID.clear();
	vkpt.clear();
	vdesc.release();

	vptID.clear();
	vpt.clear();

	ID = NOID;
}

void sBAData::clear()
{
	vkeyframe.clear();
	vkeyframeID.clear();
	vpt3d.clear();
	vvisibleID.clear();
}

MapData::MapData()
{
	Clear();
}

void MapData::Clear()
{
	mMapMutex.lock();

	mvPt.clear();
	mvKf.clear();
	mAdded = false;

	mMapMutex.unlock();
}

bool MapData::CopytoBA(sBAData &data)
{
	if (mAdded){

		mMapMutex.lock();

		if (mvPt.size() == 0){
			return false;
		}

		int prevsize = int(data.vpt3d.size());
		data.vpt3d.resize(mvPt.size());
		for (int i = prevsize, iend = int(mvPt.size()); i < iend; ++i){
			data.vpt3d[i] = mvPt[i];
		}

		int size = int(mvKf.size()) > params.BAKEYFRAMES ? params.BAKEYFRAMES : int(mvKf.size());

		data.vkeyframe.clear();
		data.vkeyframeID.clear();

		data.vkeyframe.resize(size);
		data.vkeyframeID.resize(size);

		for (int i = 0; i < size; ++i){
			int id = int(mvKf.size()) - size + i;
			data.vkeyframeID[i] = id;
			data.vkeyframe[i].pose = mvKf[id].pose;
			data.vkeyframe[i].vpt = mvKf[id].vpt;
			data.vkeyframe[i].vptID = mvKf[id].vptID;
		}

		mAdded = false;

		mMapMutex.unlock();

		return true;
	}
	else{
		return false;
	}
}

void MapData::CopyfromBA(const sBAData &data)
{
	if (mvPt.size() != 0){

		mMapMutex.lock();
		for (int i = 0, iend = int(data.vvisibleID.size()); i < iend; ++i){
			int id = data.vvisibleID[i];
			mvPt[id] = data.vpt3d[id];
		}

		for (int i = 0, iend = int(data.vkeyframeID.size()); i < iend; ++i){
			int id = data.vkeyframeID[i];
			mvKf[id].pose = data.vkeyframe[i].pose;
		}
		mMapMutex.unlock();
	}
}

void MapData::AddKeyframe(const sKeyframe &kf, const cv::Mat &vdesc)
{

	mMapMutex.lock();

	mvKf.push_back(kf);

	sKeyframe &lkf = mvKf.back();

	lkf.ID = int(mvKf.size()) - 1;		// set keyframe ID
	vdesc.copyTo(lkf.vdesc);

	mAdded = true;

	LOGOUT("%d points in keyframes %d at %s\n", lkf.vkpt.size(), lkf.ID, __FUNCTION__);

	mMapMutex.unlock();
}

sKeyframe& MapData::GetLastKeyframe()
{
	return mvKf.back();
}

void MapData::UpdateLastKeyframe(
	const std::vector<cv::Point3f> &vpt3d,
	const std::vector<cv::KeyPoint> &vkpt,
	const cv::Mat &vdesc,
	std::vector<int> &vid
	)
{
	mMapMutex.lock();
	// get last keyframe
	sKeyframe &lkf = mvKf.back();

	// get point ID
	int pointID = int(mvPt.size());

	// set point ID to points in the keyframe
	for (int i = 0, iend = int(vpt3d.size()); i < iend; ++i, ++pointID){
		mvPt.push_back(vpt3d[i]);

		lkf.vkpt.push_back(vkpt[i]);
		lkf.vkptID.push_back(pointID);

		lkf.vpt.push_back(vkpt[i].pt);
		lkf.vptID.push_back(pointID);

		vid.push_back(pointID);
	}

	// compute descriptor
	lkf.vdesc.push_back(vdesc);

	LOGOUT("Added %d points Total keypoints %d in keyframe %d\n", int(vpt3d.size()), lkf.vkpt.size(), lkf.ID);

	mMapMutex.unlock();
}

void MapData::UpdateLastKeyframe(
	const std::vector<cv::Point3f> &vpt3d,
	const std::vector<cv::KeyPoint> &vkpt,
	const cv::Mat &vdesc
	)
{

	// get last keyframe
	sKeyframe &lkf = mvKf.back();

	// get point ID
	int pointID = int(mvPt.size());

	// set point ID to points in the keyframe
	for (int i = 0, iend = int(vpt3d.size()); i < iend; ++i, ++pointID){
		mvPt.push_back(vpt3d[i]);

		lkf.vkpt.push_back(vkpt[i]);
		lkf.vkptID.push_back(pointID);

		lkf.vpt.push_back(vkpt[i].pt);
		lkf.vptID.push_back(pointID);

	}

	// compute descriptor
	lkf.vdesc.push_back(vdesc);

	LOGOUT("Added %d points Total keypoints %d in keyframe %d\n", int(vpt3d.size()), lkf.vkpt.size(), lkf.ID);

}

const cv::Point3f& MapData::GetPoint(const int id) const
{
	return mvPt[id];
}

const sKeyframe& MapData::GetNearestKeyframe(const sPose &pose) const
{
	double dist = cv::norm(mvKf[0].pose.tvec - pose.tvec);
	int ID = 0;

	for (int i = 1, iend = int(mvKf.size()); i < iend; ++i){
		double tmp = cv::norm(mvKf[i].pose.tvec - pose.tvec);
		if (tmp < dist){
			dist = tmp;
			ID = i;
		}
	}

	return mvKf[ID];
}

void MapData::GetPoseforRelocalization(sPose &pose) const
{
	int size = params.RELOCALHIST > int(mvKf.size()) ? int(mvKf.size()) : params.RELOCALHIST;
	int val = rand() % size;
	int index = int(mvKf.size()) - 1 - val;

	pose = mvKf[index].pose;
}

void MapData::GetGoodPoseforRelocalization(sPose &pose) const
{
	int size = params.RELOCALHIST > int(mvKf.size()) ? int(mvKf.size()) : params.RELOCALHIST;

	// select keyframe depending on number of keypoints in keyframe
	int maxPts = -1;
	for (int i = int(mvKf.size()) - size; i < int(mvKf.size()); ++i){
		int pts = int(mvKf[i].vkpt.size());

		if (maxPts < pts){
			maxPts = pts;
			pose = mvKf[i].pose;
		}
	}
}

int MapData::GetSize() const
{
	return int(mvPt.size());
}

Data::Data()
{
	clear();
}

void Data::clear()
{
	clearTrack();

	baData.clear();
	map.Clear();
	vKpt.clear();

	havescale = false;
}

void Data::clearTrack()
{
	vtrack.clear();
	vprevpt.clear();
}

void Data::clearTrack(const int ID)
{
	for (std::list<sTrack>::iterator it = vtrack.begin(), itend = vtrack.end(); it != itend;){
		if (it->ptID == ID){
			it = vtrack.erase(it);
		}
		else{
			++it;
		}
		if (it == vtrack.end()){
			break;
		}
	}

	vprevpt.clear();

	for (std::list<sTrack>::iterator it = vtrack.begin(), itend = vtrack.end(); it != itend; ++it){
		vprevpt.push_back(it->vpt.back());
	}
}

void Data::addTrack(const sTrack &in)
{
	const cv::Point2f &pt = in.kpt.pt;
	vtrack.push_back(in);
	vtrack.back().vpt.push_back(pt);
	vprevpt.push_back(pt);
}

Params::Params()
{
	MAXPTS = 400;
	LEVEL = 2;
	DESCDIST = 50.f;

	BASEANGLE = 2.5;
	BASETAN = tan(BASEANGLE / 180.0*CV_PI);
	BAKEYFRAMES = 5;

	PROJERR = 3.f;
	MINPTS = 20;
	PATCHSIZE = 17;
	MATCHKEYFRAME = 0.4f;

	GOODINIT = 0.9f;
	RELOCALHIST = 5;
}
