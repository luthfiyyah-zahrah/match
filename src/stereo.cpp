#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <iostream>

#include "match/StereoMatching.h"
// #include "match/compute_pose.h"
#include "match/define_struct.h"
#include "match/Visualize.h"
#include "match/online_sfm.h"

//declare
float scale = 3.0;
std::vector<std::vector<cv::Mat> > rmap(2,std::vector<cv::Mat>(2));
cv::Mat Q;
std::vector<cv::Mat> P(2);
cv::Mat cameraMatrix;
cv::Mat distCoeffs = (cv::Mat_<double>(5, 1) << 0, 0, 0, 0, 0);
cv::Size imgsize;
std::vector<cv::Mat> img_g(2);
std::vector<cv::Mat> img(2);
void ReadCameraParameter(cv::Mat &newcammat,std::vector<std::vector<cv::Mat> > &rmap, cv::Mat &Q, std::vector<cv::Mat> &P, cv::Size &imgsize, float scale)
	{
	  std::vector<cv::Mat> distCoeffs(2);
	  std::vector<cv::Mat> R(2);
	  cv::Mat cameraMatrix1, cameraMatrix2, P1, P2;

	  cv::Size ImageSize;

	  cv::FileStorage fs1("/home/zahrah/catkin_ws/src/match/parameter/intrinsics.yml", cv::FileStorage::READ);
	  cv::FileStorage fs2("/home/zahrah/catkin_ws/src/match/parameter/extrinsics.yml", cv::FileStorage::READ);

	  fs1["M1"] >> cameraMatrix1;
	  fs1["M2"] >> cameraMatrix2;
	  fs1["D1"] >> distCoeffs[0];
	  fs1["D2"] >> distCoeffs[1];

	  fs2["R1"] >> R[0];
	  fs2["R2"] >> R[1];
	  fs2["P1"] >> P1;
	  fs2["P2"] >> P2;
	  fs2["ImageSize"] >> ImageSize;

	  cv::Mat camMat1 = (cv::Mat_<double>(3, 3) << cameraMatrix1.at<double>(0, 0) / scale, 0, cameraMatrix1.at<double>(0, 2) / scale,
	    0, cameraMatrix1.at<double>(1, 1) / scale, cameraMatrix1.at<double>(1, 2) / scale,
	    0, 0, 1);
	  cv::Mat camMat2 = (cv::Mat_<double>(3, 3) << cameraMatrix2.at<double>(0, 0) / scale, 0, cameraMatrix2.at<double>(0, 2) / scale,
	    0, cameraMatrix2.at<double>(1, 1) / scale, cameraMatrix2.at<double>(1, 2) / scale,
	    0, 0, 1);
	  P[0] = (cv::Mat_<double>(3, 4) << P1.at<double>(0, 0) / scale, 0, P1.at<double>(0, 2) / scale, 0,
	    0, P1.at<double>(1, 1) / scale, P1.at<double>(1, 2) / scale, 0,
	    0, 0, 1, 0);
	  P[1] = (cv::Mat_<double>(3, 4) << P2.at<double>(0, 0) / scale, 0, P2.at<double>(0, 2) / scale, P2.at<double>(0, 3) / scale,
	    0, P2.at<double>(1, 1) / scale, P2.at<double>(1, 2) / scale, 0,
	    0, 0, 1, 0);
	  imgsize = cv::Size(ImageSize.width / scale, ImageSize.height / scale);
	  // imgsize=ImageSize;
	  newcammat = (cv::Mat_<double>(3, 3) << P[0].at<double>(0, 0), 0, P[0].at<double>(0, 2),
	    0, P[0].at<double>(1, 1), P[0].at<double>(1, 2),
	    0, 0, 1);
	  Q = (cv::Mat_<double>(4, 4) << 1, 0, 0, -P[0].at<double>(0, 2),
	    0, 1, 0, -P[0].at<double>(1, 2),
	    0, 0, 0, P[0].at<double>(0, 0),
	    0, 0, -(P[1].at<double>(0, 0) / (16 * P[1].at<double>(0, 3))), 0);/////16‚ÍŽ‹·‚Ì’l‚ð1/16‚É‚µ‚È‚¢‚Æ‚¢‚¯‚È‚¢‚©‚çDstereoBM‚Åo‚éŽ‹·‚ÍfloatŒ^‚ÅŽÀÛ‚ÌŽ‹·‚©‚ç16”{‚³‚ê‚Ä‚¢‚é
	  //Precompute maps for cv::remap()
	    cv::initUndistortRectifyMap(camMat1, distCoeffs[0], R[0], P[0], imgsize, CV_16SC2, rmap[0][0], rmap[0][1]);
	    cv::initUndistortRectifyMap(camMat2, distCoeffs[1], R[1], P[1], imgsize, CV_16SC2, rmap[1][0], rmap[1][1]);

	  fs1.release();
	  fs2.release();
	}

void GrabStereo(const sensor_msgs::ImageConstPtr& msgLeft,const sensor_msgs::ImageConstPtr& msgRight)
{
	// Copy the ros image message to cv::Mat.
	cv_bridge::CvImageConstPtr cv_ptrLeft;
	try
	{
	cv_ptrLeft = cv_bridge::toCvShare(msgLeft);
	// cv::imshow("left",cv_ptrLeft->image);
 //    cv::waitKey(30);
      
	}
	catch (cv_bridge::Exception& e)
	{
	    ROS_ERROR("cv_bridge exception: %s", e.what());
	    return;
	}

	cv_bridge::CvImageConstPtr cv_ptrRight;
	try
	{
	    cv_ptrRight = cv_bridge::toCvShare(msgRight);
	    // cv::imshow("right",cv_ptrRight->image);
    	// cv::waitKey(30);
    }
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
	    return;
	}
	//deklarasi
	ReadCameraParameter(cameraMatrix, rmap, Q, P, imgsize, scale);
	

	cv::Mat imLeft, imRight;
	img[0]=cv_ptrLeft->image;
	img[1]=cv_ptrRight->image;
	std::cout<<"image size :"<<imgsize<<std::endl;
	for (int i = 0; i < img.size(); ++i){
			cv::resize(img[i], img[i], imgsize);
			remap(img[i], img[i], rmap[i][0], rmap[i][1], CV_INTER_LINEAR);
			cv::cvtColor(img[i], img_g[i], CV_BGR2GRAY);
	}

	StereoMatching stereo=StereoMatching(Q, imgsize);
	// compute_pose compose(cameraMatrix, distCoeffs, imgsize, P[1].at<double>(0, 3) / (cameraMatrix.at<double>(0, 0)));
	online_sfm sfm(cameraMatrix, distCoeffs, imgsize, P[1].at<double>(0, 3) / (cameraMatrix.at<double>(0, 0)));
	Visualize display_3d;
	std::cout<<"here"<<std::endl;
	sfm.run(img_g);
		if (sfm.DoingStereo){
			stereo.run(img_g, img[0]);
			stereo.createPointCloud(img[0], sfm.left_cam_pose.R, sfm.left_cam_pose.tvec);
	
			sfm.DoingStereo = false;
			display_3d.draw_path = true;
			display_3d.Path_area = stereo.Path_area;
		}
		display_3d.run(sfm.left_cam_pose.rvec, sfm.left_cam_pose.tvec, stereo.point_cloud);

		imshow("disparity", stereo.disp_vis);

		sfm.draw(img[0]);
		imshow("left", img[0]);
	
		
	
	// compose.DoingStereo=true;
	// if (compose.DoingStereo)
	// {
	// 	std::cout<<"here1"<<std::endl;
	// 	stereo.run(img_g,img_g[0]);

	// 	// std::cout<<compose.left_cam_pose.R<<std::endl;
	// 	// // , sfm.left_cam_pose.tvec
	// 	// compose.DoingStereo = false;
	// }
		// stereo.run(img_g,img_g[0]);


	// imshow("disparity", stereo.disp_vis);
	// std::cout<<"here finish"<<std::endl;
}
int main(int argc, char** argv)
{
	ros::init(argc, argv, "stereomatching");

	cv::namedWindow("left");
  	cv::moveWindow("left", 700, 0);
  	cv::namedWindow("right");
  	cv::moveWindow("right", 700, imgsize.height + 740);
  	cv::namedWindow("disparity");
  	cv::moveWindow("disparity", 700, imgsize.width + 740);
  	// ImageConverter ic();
	
	ros::NodeHandle nh;
	message_filters::Subscriber<sensor_msgs::Image> left_sub(nh, "/left/image_raw", 1);
  	message_filters::Subscriber<sensor_msgs::Image> right_sub(nh, "/right/image_raw", 1);
  	typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
    message_filters::Synchronizer<sync_pol> sync(sync_pol(10), left_sub,right_sub);
    sync.registerCallback(boost::bind(&GrabStereo,_1,_2));

	ros::spin();
	return 0;
}