#include"match/PointDetector3.h"


PointDetector::PointDetector()
{
	// initialize detector and matcher
	mDetector = cv::ORB::create();
	mMatcher = cv::DescriptorMatcher::create("BruteForce-Hamming(2)");
}

PointDetector::~PointDetector()
{
}

void PointDetector::Init(
	const int numpts,
	const int numlevel
	)
{
	mDetector = cv::ORB::create(numpts, 1.2f, numlevel);
}

void PointDetector::Match(
	const cv::Mat &query,
	const cv::Mat &train,
	std::vector<cv::DMatch> &vmatch
	) const
{
	mMatcher->match(query, train, vmatch);
}

void PointDetector::Describe(
	const cv::Mat &img,
	std::vector<cv::KeyPoint> &vkpt,
	cv::Mat &vdesc
	) const
{
	mDetector->compute(img, vkpt, vdesc);
}

void PointDetector::Detect(
	const cv::Mat &img,
	std::vector<cv::KeyPoint> &vkpt
	) const
{
	mDetector->detect(img, vkpt);
}

void PointDetector::Detect(
	const cv::Mat &img,
	std::vector<cv::KeyPoint> &vkpt,
	const cv::Mat &mask
	) const
{
	mDetector->detect(img, vkpt, mask);
}