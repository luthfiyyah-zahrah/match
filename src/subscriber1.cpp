#include <ros/ros.h>
// #include <image_transport/image_transport.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>


#include "opencv2/highgui/highgui.hpp"
#include <cv_bridge/cv_bridge.h>

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

#include <iostream>

#include "match/StereoMatching.h"


namespace enc = sensor_msgs::image_encodings;
class ImageGrabber
{
public:
    ImageGrabber(){}
    void GrabStereo(const sensor_msgs::ImageConstPtr& msgLeft,const sensor_msgs::ImageConstPtr& msgRight);
    cv::Mat M1l,M2l,M1r,M2r;
    
};
void imageCallbackright(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImageConstPtr cv_ptr;
  try
  {
    // cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);
    cv_ptr = cv_bridge::toCvShare(msg, enc::BGR8);
    cv::imshow("right",cv_ptr->image);
    cv::waitKey(30);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }

  /*cv::circle(cv_ptr->image, cv::Point(50, 50), 10, CV_RGB(255,0,0));
  cv::imshow("left", cv_ptr->image);
  cv::waitKey(30);*/
}
 void imageCallbackleft(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImageConstPtr cv_ptr;
  try
  {
    // cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);
    cv_ptr = cv_bridge::toCvShare(msg, enc::BGR8);
    cv::imshow("left",cv_ptr->image);
    cv::waitKey(30);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }

  /*cv::circle(cv_ptr->image, cv::Point(50, 50), 10, CV_RGB(255,0,0));
  cv::imshow("left", cv_ptr->image);
  cv::waitKey(30);*/
}

void ReadCameraParameter(cv::Mat &newcammat,std::vector<std::vector<cv::Mat> > &rmap, cv::Mat &Q, std::vector<cv::Mat> &P, cv::Size &imgsize, float scale)
{
  std::vector<cv::Mat> distCoeffs(2);
  std::vector<cv::Mat> R(2);
  cv::Mat cameraMatrix1, cameraMatrix2, P1, P2;

  cv::Size ImageSize;

  cv::FileStorage fs1("/home/luthfi/catkin_ws/src/match/parameter/intrinsics.yml", cv::FileStorage::READ);
  cv::FileStorage fs2("/home/luthfi/catkin_ws/src/match/parameter/extrinsics.yml", cv::FileStorage::READ);

  fs1["M1"] >> cameraMatrix1;
  fs1["M2"] >> cameraMatrix2;
  fs1["D1"] >> distCoeffs[0];
  fs1["D2"] >> distCoeffs[1];

  fs2["R1"] >> R[0];
  fs2["R2"] >> R[1];
  fs2["P1"] >> P1;
  fs2["P2"] >> P2;
  fs2["ImageSize"] >> ImageSize;

  cv::Mat camMat1 = (cv::Mat_<double>(3, 3) << cameraMatrix1.at<double>(0, 0) / scale, 0, cameraMatrix1.at<double>(0, 2) / scale,
    0, cameraMatrix1.at<double>(1, 1) / scale, cameraMatrix1.at<double>(1, 2) / scale,
    0, 0, 1);
  cv::Mat camMat2 = (cv::Mat_<double>(3, 3) << cameraMatrix2.at<double>(0, 0) / scale, 0, cameraMatrix2.at<double>(0, 2) / scale,
    0, cameraMatrix2.at<double>(1, 1) / scale, cameraMatrix2.at<double>(1, 2) / scale,
    0, 0, 1);
  P[0] = (cv::Mat_<double>(3, 4) << P1.at<double>(0, 0) / scale, 0, P1.at<double>(0, 2) / scale, 0,
    0, P1.at<double>(1, 1) / scale, P1.at<double>(1, 2) / scale, 0,
    0, 0, 1, 0);
  P[1] = (cv::Mat_<double>(3, 4) << P2.at<double>(0, 0) / scale, 0, P2.at<double>(0, 2) / scale, P2.at<double>(0, 3) / scale,
    0, P2.at<double>(1, 1) / scale, P2.at<double>(1, 2) / scale, 0,
    0, 0, 1, 0);
  imgsize = cv::Size(ImageSize.width / scale, ImageSize.height / scale);
  newcammat = (cv::Mat_<double>(3, 3) << P[0].at<double>(0, 0), 0, P[0].at<double>(0, 2),
    0, P[0].at<double>(1, 1), P[0].at<double>(1, 2),
    0, 0, 1);
  Q = (cv::Mat_<double>(4, 4) << 1, 0, 0, -P[0].at<double>(0, 2),
    0, 1, 0, -P[0].at<double>(1, 2),
    0, 0, 0, P[0].at<double>(0, 0),
    0, 0, -(P[1].at<double>(0, 0) / (16 * P[1].at<double>(0, 3))), 0);/////16‚ÍŽ‹·‚Ì’l‚ð1/16‚É‚µ‚È‚¢‚Æ‚¢‚¯‚È‚¢‚©‚çDstereoBM‚Åo‚éŽ‹·‚ÍfloatŒ^‚ÅŽÀÛ‚ÌŽ‹·‚©‚ç16”{‚³‚ê‚Ä‚¢‚é
  //Precompute maps for cv::remap()
    cv::initUndistortRectifyMap(camMat1, distCoeffs[0], R[0], P[0], imgsize, CV_16SC2, rmap[0][0], rmap[0][1]);
    cv::initUndistortRectifyMap(camMat2, distCoeffs[1], R[1], P[1], imgsize, CV_16SC2, rmap[1][0], rmap[1][1]);

  fs1.release();
  fs2.release();
}
void GrabStereo(const sensor_msgs::ImageConstPtr& msgLeft,const sensor_msgs::ImageConstPtr& msgRight)
{
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptrLeft;
    try
    {
        cv_ptrLeft = cv_bridge::toCvShare(msgLeft);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv_bridge::CvImageConstPtr cv_ptrRight;
    try
    {
        cv_ptrRight = cv_bridge::toCvShare(msgRight);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    // ini agak aneh
    cv::Mat imLeft, imRight;
    cv::remap(cv_ptrLeft->image,imLeft,rmap[0][0],rmap[0][1],cv::INTER_LINEAR);
    cv::remap(cv_ptrRight->image,imRight,rmap[1][0],rmap[1][1],cv::INTER_LINEAR);
    

    //deklarasi stereo
    stereo.run(imLeft, imRight);   


    cv::imshow("left",cv_ptrLeft->image);
    cv::waitKey(30);
    cv::imshow("right",cv_ptrRight->image);
    cv::waitKey(30);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "image_listener");
  ros::NodeHandle nh;
  
  //salah deklarasi klass
  ImageGrabber igb();
  
  float scale = 3.0;
  std::vector<std::vector<cv::Mat> > rmap(2,std::vector<cv::Mat>(2));
  cv::Mat Q;
  std::vector<cv::Mat> P(2);
  cv::Mat cameraMatrix;
  cv::Mat distCoeffs = (cv::Mat_<double>(5, 1) << 0, 0, 0, 0, 0);
  cv::Size imgsize;

  //igb.rmapnyagak bisa dibaca
  ReadCameraParameter(cameraMatrix, rmap, Q, P, imgsize, scale);
  
  // image process
  cv::namedWindow("left");
  cv::moveWindow("left", 700, 0);
  cv::namedWindow("right");
  cv::moveWindow("right", 700, imgsize.height + 40);
  
  std::cout<<"Q: "<<Q<<std::endl;
  StereoMatching stereo(Q, imgsize);
  
  // image_transport::ImageTransport it(nh);
  // image_transport::Subscriber subka = it.subscribe("right/image_raw", 1, imageCallbackright);
  // image_transport::Subscriber subki = it.subscribe("left/image_raw", 1, imageCallbackleft);
  message_filters::Subscriber<sensor_msgs::Image> left_sub(nh, "left/image_raw", 1);
  message_filters::Subscriber<sensor_msgs::Image> right_sub(nh, "camera/right/image_raw", 1);
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
  message_filters::Synchronizer<sync_pol> sync(sync_pol(10), left_sub,right_sub);
  //->aneh , masih salah
  sync.registerCallback(GrabStereo);



  ros::spin();
 

  cv::destroyAllWindows();
  ros::shutdown();

  return 0;
}
